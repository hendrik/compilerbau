type exp =
  | IdExp of string
  | NumExp of int
  | OpExp of exp * binop * exp
  | EseqExp of stm * exp
and stm =
  | AssignStm of string * exp
  | CompoundStm of stm * stm
  | PrintStm of exp list
and binop =
  | PLUS
  | MINUS
  | TIMES
  | DIVIDE

module Env = Map.Make(String)
type env = int Env.t

let sample = CompoundStm(
    AssignStm("a",
      OpExp(NumExp(5), PLUS, NumExp(3))),
    CompoundStm(AssignStm("b",
        EseqExp(PrintStm([IdExp("a");
                          OpExp(IdExp("a"), MINUS, NumExp(1))]),
          OpExp(NumExp(10), TIMES, IdExp("a")))),
      PrintStm([IdExp("b")])))

let rec prettyprint stm =
  match stm with
  | CompoundStm(a, b) -> prettyprint a ^ "; " ^ prettyprint b
  | AssignStm(i, e) -> i ^ " = " ^ prettyprint_exp e
  | PrintStm(e) -> "print (" ^ String.concat ", " (List.map prettyprint_exp e) ^ ")"
and prettyprint_exp exp =
  match exp with
  | IdExp(i) -> i
  | NumExp(n) -> string_of_int n
  | OpExp(a, o, b) -> prettyprint_exp a ^ prettyprint_op o ^ prettyprint_exp b
  | EseqExp(s, e) -> "(" ^ (prettyprint s) ^ ", " ^ (prettyprint_exp e) ^ ")"
and prettyprint_op op =
  match op with
  | PLUS -> "+"
  | MINUS -> "-"
  | TIMES -> "*"
  | DIVIDE -> "/"

let id x = x

let rec eval_stm stm k =
  match stm with
  | AssignStm(s, e) -> eval_exp e (fun v eenv' -> k (Env.add s v eenv'))
  | PrintStm(e) ->
    let rec iter xs = match xs with
      | [] ->
        print_string "\n";
        k
      | x::xs ->
        eval_exp x (fun v ->
          print_int v;
          print_string " ";
          iter xs)
    in iter e
  | CompoundStm(a, b) ->
    eval_stm a (eval_stm b k)
and eval_exp exp k =
  match exp with
  | NumExp(n) -> k n
  | IdExp(i) -> fun eenv -> (k (Env.find i eenv) eenv)
  | EseqExp(s, e) ->
    eval_stm s (eval_exp e k)
  | OpExp(a, o, b) ->
    eval_exp a (fun a' ->
      eval_exp b (fun b' ->
        k (eval_op o a' b')))
and eval_op o =
  match o with
  | PLUS -> (+)
  | MINUS -> (-)
  | TIMES -> ( * )
  | DIVIDE -> (/)
and eval stm = eval_stm stm id Env.empty

let main () =
  print_string (prettyprint sample);
  print_string "\n";
  eval sample

let _ = main ()
