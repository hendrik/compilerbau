
module Main where

import Control.Monad
import Data.List -- find
import qualified Data.Map as Map
import System.Environment
import System.IO
import System.Console.GetOpt
import Text.PrettyPrint

-- import Access -- Frame
import Util -- Pretty

import Frame
import FrameIntel
import LexIntel
import ParseIntel
import StateIntel

data Flag = Verbose
          | ShowAST 
          | Help 
          | Traces String
          | CallT
          | JumpT
          | MoveT
          | AllocT
          | ParT
          | RegM
          | MemM
          | LabM
          | Debug
          | FP String
          | SP String
          | SSZ String
          | HSZ String
          | HP String
          | RP String
          | StdCallConv
          | Warnings
          | Output String
            deriving (Eq, Show)

optDescrs :: [OptDescr Flag]
optDescrs =
  [ Option ['?'] ["help"]      (NoArg Help)     "show usage information"
  , Option ['v'] ["verbose"]   (NoArg Verbose) "be verbose"
  , Option ['a'] ["ast"]       (NoArg ShowAST) "show AST"
  , Option ['T'] ["defTrace"]  (ReqArg Traces "Traces") "default tracing flags (NOT IMPLEMENTED)"
  , Option ['C'] ["callTrace"] (NoArg CallT) "trace function calls"
  , Option ['J'] ["jumpTrace"] (NoArg JumpT) "trace jumps"
  , Option ['M'] ["moveTrace"] (NoArg MoveT) "trace moves"
  , Option ['A'] ["allocTrace"](NoArg AllocT) "trace memory allocations"
  , Option ['P'] ["paramTrace"](NoArg ParT) "trace parameter passing"
  , Option ['R'] ["regMap"]    (NoArg RegM) "print register maps"
  , Option ['Q'] ["memMap"]    (NoArg MemM) "print memory maps"
  , Option ['L'] ["labMap"]    (NoArg LabM) "print label maps"
  , Option ['D'] ["debugging"] (NoArg Debug) "debug interpreter itself"
  , Option ['S'] ["stacksize"]     (ReqArg SSZ "size") "stack size"
  , Option ['H'] ["heapsize"]      (ReqArg HSZ "size") "heap size"
  , Option ['W'] ["warnings"]      (NoArg Warnings) "print warnings"
  , Option ['z'] ["stdcallconv"]   (NoArg StdCallConv) "standard calling convention (default: all regs are callee saved)"
  ]


usage :: [String] -> IO a 
usage _ = error "Usage: risc386 [options] <file>" 

parseCmdLine :: [String] -> IO String
parseCmdLine argv = do
  let (os, ns, _) = getOpt Permute optDescrs argv
  if (length ns)<1
    then usage [""]
    else return ()
  let (prgFile:_) = ns
  when (Verbose `elem` os) $ hPutStrLn stderr $ "Reading program from file: " ++ prgFile
  when (Help `elem` os) $ ioError (userError "No execution with help")
  return prgFile 


{- main:
  * parse IntelFrame list
  * search "*main" function
  * split frames into blocks IBlockFrame
  * convert frame list into a map  
  * execute main function
 -}
main :: IO ()
main =	do
  cmdLine  <- getArgs
  prgFile <- parseCmdLine cmdLine
  input <- readFile prgFile
  -- hPutStrLn stderr $ "Passing these args to main fct: " ++ (show args)
  -- input <- getContents
  -- print (alexScanTokens input)
  let frames = parse (alexScanTokens input)
  lmain <- maybeM "no main function found" $
             find (\ l -> drop (length l - 4) l == "main") 
                  (map frameName frames)
  hPutStrLn stderr ("## Input:\n" ++ (render $ ppr frames))

  let bfs = map iBlocksFrame frames
  hPutStrLn stderr ("## Blocks:\n" ++ (render $ ppr bfs))

  let framemap =  Map.fromList $ map (\ f@(IBlockFrame l _ _) -> (l,f)) $ bfs
  output <- run framemap lmain 
  mapM_ putStrLn output



{-
  let rx = mkRegex "^.*\\$"
  -- let xs = map (\ (AbsSyn.Fct name _ _) -> (subRegex rx name "")) ast
  -- putStrLn (unlines xs)
  let main_ast = case (find (\ (AbsSyn.Fct name _ _ _) -> (subRegex rx name "") == "main") ast) of 
   		   Nothing -> error "PANIC: no main function found" 
   		   (Just main_ast) -> main_ast 
-}
