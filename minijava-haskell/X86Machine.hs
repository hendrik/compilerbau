{-# LANGUAGE MultiParamTypeClasses, GeneralizedNewtypeDeriving,
 InstanceSigs #-}
module X86Machine where

import Control.Monad
import Control.Monad.Trans
import Control.Monad.Trans.Reader
import Data.Char (toLower)
import Data.Maybe
import MachineSpecifics
import Names
import Tree hiding (MOVE,AND,OR,XOR,CALL,NE,LE,GE)
import qualified Tree
import qualified Data.List as List

data ArithExp
  = EInt Int
  | EPlus ArithExp ArithExp
  | EMinus ArithExp ArithExp

instance Show ArithExp where
  show (EInt i)       = show i
  show (EPlus e1 e2)  = show e1 ++ " + " ++ show e2
  show (EMinus e1 e2) = show e1 ++ " - " ++ show e2

data Mem = Mem { base :: Op
               , offset :: Maybe (Op,Int)
               , displacement :: Int
               }

data Op
  = OImm Int
  | OReg Temp
  | OMem Mem

useOp :: Op -> [Temp]
useOp (OImm _) = []
useOp (OReg t) = [t]
useOp (OMem m) = useOp (base m) ++ (maybe [] (useOp . fst) (offset m))

makeMem :: Op -> Op
makeMem base = OMem $ Mem base Nothing 0

makeMemIndirect :: Op -> Op -> Int-> Int -> Op
makeMemIndirect base index scale disp = OMem $ Mem base (Just (index,scale)) disp

renameOp :: Op -> (Temp -> Temp) -> Op
renameOp (OImm i) _   = OImm i
renameOp (OReg t) fun = OReg (fun t)
renameOp (OMem m) fun = OMem $ renameMem m
 where
   renameMem (Mem b o d) = Mem (renameOp b fun)
     (maybe Nothing (\(op,sc) -> Just (renameOp op fun,sc)) o) d

eaxT = mkNamedTemp "%eax"
ebxT = mkNamedTemp "%ebx"
ecxT = mkNamedTemp "%ecx"
edxT = mkNamedTemp "%edx"
esiT = mkNamedTemp "%esi"
ediT = mkNamedTemp "%edi"
espT = mkNamedTemp "%esp"
ebpT = mkNamedTemp "%ebp"

eax = OReg eaxT
ebx = OReg ebxT
ecx = OReg ecxT
edx = OReg edxT
esi = OReg esiT
edi = OReg ediT
esp = OReg espT
ebp = OReg ebpT

instance Show Mem where
  show m = "DWORD PTR [" ++ show (base m) ++
    maybe [] (\(t,s) -> "+" ++ show t ++ "*" ++ show s) (offset m) ++
    (\s -> case () of 
      _ | s < 0  -> show s
        | s > 0  -> "+" ++ show s
        | s == 0 -> []) (displacement m)
    ++ "]"
    

instance Show Op where
  show (OImm i) = show i
  show (OReg t) = show t
  show (OMem m) = show m
 
data X86Assem
  = AsmJump   Jump   Label
  | AsmUnary  Unary  Op
  | AsmBinary Binary Op    Op
  | AsmInstr  Instr
  | AsmLabel  Label

instance Show X86Assem where
  show (AsmJump j l) = "AsmJump " ++ show j ++ " " ++ l
  show (AsmUnary u o) = "AsmUnary " ++ show u ++ " " ++ show o
  show (AsmBinary b o1 o2) = "AsmBinary " ++ show b ++ " " ++ show o1 ++ " " ++ show o2
  show (AsmInstr i) = "AsmInstr " ++ show i
  show (AsmLabel l) = "AsmLabel " ++ l


data Jump
  = CALL
  | J Cond
  | JMP
  deriving (Show)

data Unary
  = IDIV
  | POP
  | PUSH
  deriving (Show)

data Binary
  = ADD
  | AND
  | CMP
  | IMUL
  | MOV
  | OR
  | SAL
  | SAR
  | SHL
  | SHR
  | SUB
  | XOR
  deriving (Show)

data Instr
  = NOP
  | RET
  | CDQ
  deriving (Show)

data Cond
  = A
  | AE
  | B
  | BE
  | E
  | G
  | GE
  | L
  | LE
  | NE
  deriving (Show)

retToCond :: RelOp -> Cond
retToCond Tree.EQ  = E
retToCond Tree.NE  = NE
retToCond Tree.LT  = L
retToCond Tree.GT  = G
retToCond Tree.LE  = LE
retToCond Tree.GE  = GE
retToCond Tree.ULT = B
retToCond Tree.ULE = BE
retToCond Tree.UGT = A
retToCond Tree.UGE = AE

data X86Frame = X86Frame { fName :: String
                         , fParams :: [Temp]
                         , fLocals :: Int
                         , retTemp :: Temp
                         } deriving (Show)

instance Assem X86Assem where
  use = asmRead -- registers read
    where
      asmRead :: X86Assem -> [Temp]
      asmRead (AsmUnary  PUSH o)     = useOp o
      asmRead (AsmUnary  IDIV o)     = useOp o
      asmRead (AsmUnary  POP _)      = []
      asmRead (AsmBinary MOV (OReg _) o2) = useOp o2
      asmRead (AsmBinary _ o1 o2) = useOp o1 ++ useOp o2
      asmRead (AsmInstr RET)       = eaxT : []
      asmRead _                   = []
  def (AsmJump CALL _) = eaxT : ecxT : edxT : [] -- registers changed
  def (AsmJump _ _)    = []
  def (AsmUnary POP op) = useOp op
  def (AsmUnary IDIV op) = eaxT : edxT : useOp op
  def (AsmUnary PUSH op) = []
  def (AsmBinary ADD o1 _) = useOp o1
  def (AsmBinary AND o1 _) = useOp o1
  def (AsmBinary IMUL o1 _) = useOp o1
  def (AsmBinary MOV (OMem _) _) = []
  def (AsmBinary MOV o1 _) = useOp o1
  def (AsmBinary OR o1 _) = useOp o1
  def (AsmBinary SAL o1 _) = useOp o1
  def (AsmBinary SAR o1 _) = useOp o1
  def (AsmBinary SHL o1 _) = useOp o1
  def (AsmBinary SHR o1 _) = useOp o1
  def (AsmBinary SUB o1 _) = useOp o1
  def (AsmBinary XOR o1 _) = useOp o1
  def (AsmBinary _ _ _) = []
  def (AsmLabel _) = []
  def (AsmInstr _) = []
  jumps = jumpTargets -- can jump to
    where
      jumpTargets :: X86Assem -> [Label]
      jumpTargets (AsmJump JMP   l) = [l]
      jumpTargets (AsmJump (J _) l) = [l]
      jumpTargets _                 = []
  isFallThrough f = case f of -- may the part fall through
                      (AsmJump JMP _) -> False
                      (AsmInstr RET)  -> False
                      _               -> True
  isMoveBetweenTemps (AsmBinary MOV (OReg dest) (OReg src)) =
    Just (dest,src)
  isMoveBetweenTemps _ = Nothing
  isLabel f = case f of
                AsmLabel l -> Just l
                _          -> Nothing
  rename :: X86Assem -> (Temp -> Temp) -> X86Assem
  rename asm fun = case asm of
    AsmUnary u op -> AsmUnary u (renameOp op fun)
    AsmBinary u o1 o2 -> AsmBinary u (renameOp o1 fun) (renameOp o2 fun)
    _ -> asm

instance Frame X86Frame where
  name = fName
  params f = [ Tree.TEMP t | t <- fParams f ]
  size f = (length $ fParams f) + fLocals f
  allocLocal f _ = do
    t <- nextTemp
    return ( X86Frame (fName f) (fParams f) (fLocals f) (retTemp f)
           , Tree.TEMP t )
  makeProc f body retExp =
    return $ Tree.SEQ body $ Tree.MOVE (Tree.TEMP $ retTemp f) retExp

getParam f t = do
  s <- wordSize
  case List.elemIndex t (fParams f) of
    Just i  -> return $ Just $ OMem $ Mem ebp Nothing ((i+2) * s)
    Nothing -> return $ Nothing

getParamOrTemp f t = do
  p <- getParam f t
  return $ fromMaybe (OReg t) p


newtype X86MachineT m a = X86MachineT { runX86MachineT :: NameGenT m a }
  deriving (Monad, MonadNameGen, MonadTrans, Functor)

withX86Machine :: Monad m => X86MachineT m a -> m a
withX86Machine = runNameGenT . runX86MachineT

instance (Monad m) 
  => MachineSpecifics (X86MachineT m) X86Assem X86Frame where
  wordSize = return 4
  mkFrame name nparams = do
    paramsTemp <- replicateM nparams nextTemp
    returnTemp <- nextTemp
    return $ X86Frame name paramsTemp 0 returnTemp
  codeGen fr@(FragmentProc f stms) = do
    s <- wordSize
    assems <- munch fr
    return $ FragmentProc f $ 
      AsmLabel (fName f) :
      AsmUnary PUSH ebp :
      AsmBinary MOV ebp esp :
      AsmBinary SUB esp (OImm 0) : -- HACK done later...
      AsmUnary PUSH ebx :
      AsmUnary PUSH esi :
      AsmUnary PUSH edi :
      concat assems ++
      [AsmBinary MOV eax (OReg $ retTemp f)] ++
      [AsmUnary POP edi] ++
      [AsmUnary POP esi] ++
      [AsmUnary POP ebx] ++
      [AsmBinary MOV esp ebp] ++
      [AsmUnary POP ebp] ++
      [AsmInstr RET]
  allRegisters = 
    return $ Just $ espT : ebpT : eaxT : ebxT : ecxT : edxT : esiT : ediT : []
  generalPurposeRegisters = 
    return $ Just $ eaxT : ebxT : ecxT : edxT : esiT : ediT : []
  spill f asms (t:ts) = do
    (f',e) <- allocStackLocal f InMemory
    tt <- nextTemp
    next <- spill f' (rewriteAsms asms tt e) ts
    return next
    where
      rewriteAsms (a:as) t' e =
        (if t `elem` (use a) then [AsmBinary MOV (OReg t') e] else []) ++
        [rename a (\s -> if s == t then t' else s)] ++
        (if t `elem` (def a) then [AsmBinary MOV e (OReg t')] else []) ++
        rewriteAsms as t' e
      rewriteAsms []     _ _ = []
      -- hack, because allocLocal returns Tree Expression, not an Op
      allocStackLocal f _ = do
        s <- wordSize
        return ( X86Frame (fName f) (fParams f) (fLocals f + 1) (retTemp f)
               , OMem $ Mem ebp Nothing (-1*s*(1+fLocals f)) )
  spill f asms []     = return (f,asms)
  printAssembly f = return $ unlines $ map (printAssem . body) f

isTemp :: Exp -> Bool
isTemp (TEMP _) = True
isTemp _        = False

munch (FragmentProc f stms) = do
  asms  <- runReaderT (mapM munchStm stms) f
  return asms

--munchStm :: Stm -> [Asm]
munchStm (Tree.MOVE (MEM (BINOP PLUS m (CONST i))) s) = do
  (om, am) <- munchExp m
  (os, as) <- munchExp s
  return $ am ++ as ++ [AsmBinary MOV (OMem $ Mem om Nothing i) os]
munchStm (Tree.MOVE (MEM (BINOP MINUS m (CONST i))) s) = do
  (om, am) <- munchExp m
  (os, as) <- munchExp s
  return $ am ++ as ++ [AsmBinary MOV (OMem $ Mem om Nothing (-1*i)) os]
munchStm (Tree.MOVE (MEM m) s)= do
  (om, am) <- munchExp m
  (os, as) <- munchExp s
  return $ am ++ as ++ [AsmBinary MOV (makeMem om) os]
munchStm (Tree.MOVE d s) = do
  (od, ad) <- munchExp d
  (os, as) <- munchExp s
  return $ ad ++ as ++ [AsmBinary MOV od os]
munchStm (EXP e) = do
  (_, ae) <- munchExp e
  return ae
munchStm (JUMP dest poss) = do
  return [AsmJump JMP $ head poss]

munchStm (CJUMP rel left right true false) = do
  (ol, al) <- munchExp left
  (or, ar) <- munchExp right
  t <- nextTemp
  return $ al ++ ar ++
    [AsmBinary MOV (OReg t) ol] ++
    [AsmBinary CMP (OReg t) or] ++
    [(AsmJump (J $ retToCond rel) true)] ++
    [AsmJump JMP false]
munchStm (SEQ _ _) = error "found SEQ in munchStm"
munchStm (LABEL l) = return $ [AsmLabel l]
  
munchExp (CONST i) = return (OImm i, [])
munchExp (NAME l) = error "found NAME in munchExp"
munchExp (TEMP t) = do
  f <- ask
  op <- getParam f t
  case op of
    Just o  -> do
      t' <- nextTemp
      return (OReg t', [AsmBinary MOV (OReg t') o])
    Nothing -> return (OReg t, [])
munchExp (BINOP op (CONST i) (CONST j)) = do
  t <- nextTemp
  (o,a) <- munchExp (BINOP op (TEMP t) (CONST j))
  return (o, AsmBinary MOV (OReg t) (OImm i) : a)
munchExp (BINOP DIV e1 e2) = do
  (oe1,ae1) <- munchExp e1
  (oe2,ae2) <- munchExp e2
  tres <- nextTemp
  return (OReg tres, ae1 ++ ae2 ++ 
    [AsmBinary MOV eax (oe1)] ++
    [AsmInstr CDQ] ++
    [AsmUnary IDIV oe2] ++
    [AsmBinary MOV (OReg tres) eax])
munchExp (BINOP MUL e1 (CONST i)) = do
  (oe1,ae1) <- munchExp e1
  ti <- nextTemp
  tret <- nextTemp
  return (OReg tret, ae1 ++
    [AsmBinary MOV (OReg ti) (OImm i)] ++
    [AsmBinary MOV (OReg tret) oe1] ++
    [AsmBinary IMUL (OReg tret) (OReg ti)])
munchExp (BINOP op e1 e2) = do
  (oe1,ae1) <- munchExp e1
  (oe2,ae2) <- munchExp e2
  tret <- nextTemp
  return (OReg tret,ae1 ++ ae2 ++ 
    [AsmBinary MOV (OReg tret) oe1] ++
    [AsmBinary (translateOp op) (OReg tret) oe2])
  where
    translateOp :: BinOp -> Binary
    translateOp PLUS     = ADD
    translateOp MINUS    = SUB
    translateOp MUL      = IMUL
    translateOp Tree.AND = AND
    translateOp Tree.OR  = OR
    translateOp LSHIFT   = SHL
    translateOp RSHIFT   = SHR
    translateOp ARSHIFT  = SAR
    translateOp Tree.XOR = XOR
munchExp (MEM e) = do
  (oe, ae) <- munchExp e
  t <- nextTemp
  return ((OReg t), ae ++ [AsmBinary MOV (OReg t) (makeMem oe)])
munchExp (Tree.CALL fun args) = do
  (orags,aargs) <- mapAndUnzipM munchExp args
  tres <- nextTemp
  return (OReg tres, concat aargs ++
    map (AsmUnary PUSH) (reverse orags) ++
    [AsmJump CALL (nameToLabel fun)] ++
    [AsmBinary ADD esp (OImm (4*length args))] ++
    [AsmBinary MOV (OReg tres) eax])
  where
    nameToLabel :: Exp -> Label
    nameToLabel (NAME e) = e
munchExp (ESEQ _ _) = error "found ESEQ in munchExp"


indent :: String
indent = "\t"

printAssem :: [X86Assem] -> String
printAssem asm =
  unlines $ ".intel_syntax" : ".global Lmain" : "" : map printAssem' asm

printAssem' :: X86Assem -> String
printAssem' (AsmJump j l)       = indent ++ printJump j ++ " " ++ l
printAssem' (AsmUnary asm op)   = indent ++ show asm ++ " " ++ show op
printAssem' (AsmBinary asm l r) =
  indent ++ show asm ++ " " ++ show l ++ ", " ++ show r
printAssem' (AsmInstr asm)      = indent ++ show asm
printAssem' (AsmLabel l)        = l ++ ":"

printJump :: Jump -> String
printJump (J c) = "J" ++ show c
printJump j = show j

-- hack to correct esp after code generation
-- replaces all SUB esp, 0, but this should be unique
adjustEsp (FragmentProc f b) = do
  ret <- adjustEsp' b
  return $ FragmentProc f ret
  where 
    adjustEsp' ((AsmBinary SUB esp (OImm 0)):bs) = do
      s <- wordSize
      if fLocals f == 0
        then return bs
	else return $ AsmBinary SUB esp (OImm (s*(fLocals f))):bs
    adjustEsp' ((b:bs)) = do
      bs' <- adjustEsp' bs
      return $ b:bs'
    adjustEsp' _ = error "failed to correct esp offset"
