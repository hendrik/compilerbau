module TreeTranslator where

import Prelude hiding (EQ,GT,LT)
import Control.Monad (liftM, liftM2)
import Control.Monad.Trans.Class (lift)
import Control.Monad.Trans.Reader
import Data.Map (Map)
import qualified Data.Map as Map

import Minijava
import Symbols
import Names
import Tree
import MachineSpecifics

-- for debugging
(!) :: (Ord k, Show k, Show a) => Map k a -> k -> a
(!) map value =
  case Map.lookup value map of
    Just a  -> a
    Nothing -> error $ "element " ++ show value ++ " not in map.\n" ++ show map

translateBinOp :: MachineSpecifics m a f =>
  Map String (Exp, Type) -> BinOp -> Expression -> Expression
  -> ReaderT ProgramTable m Exp
translateBinOp vars b e1 e2 = do
  me1 <- translateExpression vars e1
  me2 <- translateExpression vars e2
  return $ BINOP b me1 me2

translateExpression
  :: MachineSpecifics m a f =>
       Map String (Exp, Type) -> Expression -> ReaderT ProgramTable m Exp
translateExpression vars (ELogicalAnd e1 e2) = do
  lsnd   <- nextLabel
  ltrue  <- nextLabel
  lend   <- nextLabel
  ret    <- nextTemp
  me1    <- translateConditional vars e1 lsnd  lend
  me2    <- translateConditional vars e2 ltrue lend
  return $ ESEQ (
    toSeq $ MOVE (TEMP ret) (CONST 0) : me1 : LABEL lsnd : me2 
              : LABEL ltrue : MOVE (TEMP ret) (CONST 1) : LABEL lend : []
    ) (TEMP ret)
translateExpression vars (ELessThan e1 e2)  = do
  me1    <- translateExpression vars e1
  me2    <- translateExpression vars e2
  tret   <- nextTemp
  ltrue  <- nextLabel
  lfalse <- nextLabel
  lend   <- nextLabel
  return $ ESEQ (
    toSeq $
      CJUMP LT me1 me2 ltrue lfalse : 
      LABEL ltrue : MOVE (TEMP tret) (CONST 1) : jump lend :
      LABEL lfalse :MOVE (TEMP tret) (CONST 0) : LABEL lend : []
    ) (TEMP tret)
translateExpression vars (EPlus e1 e2)      = do
  me1 <- translateExpression vars e1
  me2 <- translateExpression vars e2
  return $ plus me1 me2
translateExpression vars (EMinus e1 e2)     = do
  me1 <- translateExpression vars e1
  me2 <- translateExpression vars e2
  return $ minus me1 me2
translateExpression vars (ETimes e1 e2)     = do
  me1 <- translateExpression vars e1
  me2 <- translateExpression vars e2
  return $ times me1 me2
translateExpression vars (EDiv e1 e2)       = do
  me1 <- translateExpression vars e1
  me2 <- translateExpression vars e2
  return $ divide me1 me2
translateExpression vars (EArrayAccess v p) = do
  mv   <- translateExpression vars v
  mp   <- translateExpression vars p
  tv   <- nextTemp
  tp   <- nextTemp
  lerr <- nextLabel
  lok  <- nextLabel
  size <- wordSize
  return $ ESEQ (
    toSeq $ MOVE (TEMP tp) mp : MOVE (TEMP tv) mv :
      CJUMP GE (TEMP tp) (MEM $ TEMP tv) lerr lok :
      LABEL lerr : EXP (CALL (NAME "L_raise") [CONST 1]) :
      LABEL lok : []
   ) $ MEM $ plus (TEMP tv) $ scale (plus (CONST 1) (TEMP tp)) size
translateExpression vars (ELength v)    = translateExpression vars v >>= return . MEM
translateExpression vars (EFunc c f as) = do
  mc   <- translateExpression vars c
  mf   <- mapM (translateExpression vars) as
  tret <- nextTemp
  progTable <- ask
  return $ ESEQ (
     MOVE (TEMP tret) (CALL (NAME $ functionName progTable) (mc : mf))
    ) (TEMP tret)
  where
    functionName :: ProgramTable -> Label
    functionName p = 
      case typeOf p c of
        (TClass s) -> s ++ "$$" ++ f
        _ -> error "BUGBUG1 Function call on non class after typechecker"
    typeOf :: ProgramTable -> Expression -> Type
    typeOf _ (ENew s)        = TClass s
    typeOf p (EFunc c' f' _) = 
      case typeOf p c' of
        (TClass s) -> methTType ((classTMeths (progTClasses p ! s)) ! f')
        _ -> error "BUGBUG2 Function call on non class after typechecker"
    typeOf _ (EIdent s)        = snd $ vars ! s
    typeOf _ EThis             = snd $ vars ! "this" 
    typeOf p (EBraces e)       = typeOf p e
    -- other cases are a bug in the type checker
    typeOf _ _ = error $ "BUGBUG3 Function call on non class after typechecker while translating " ++ show (EFunc c f as)
translateExpression vars (ENum i)       = return $ CONST i
translateExpression vars (ETrue)        = return $ CONST 1
translateExpression vars (EFalse)       = return $ CONST 0
translateExpression vars (EIdent s)     = return $ fst $ vars ! s
translateExpression vars (EThis)        = return $ fst $ vars ! "this"
translateExpression vars (ENewArray e)  = do
  esize <- translateExpression vars e
  tsize <- nextTemp
  tret  <- nextTemp
  size  <- wordSize
  return $ ESEQ (
      toSeq $ MOVE (TEMP tsize) esize
        : MOVE (TEMP tret) (CALL (NAME "L_halloc") 
            [scale (plus (TEMP tsize) (CONST 1)) size])
        : MOVE (MEM $ TEMP tret) (TEMP tsize) : []
    ) (TEMP tret)
translateExpression vars (ENew c)      = do
  p <- asks progTClasses
  tret <- nextTemp
  size <- wordSize
  return $ ESEQ (
      MOVE (TEMP tret) (CALL (NAME "L_halloc") 
                             [scale (CONST $ classTSize (p ! c)) size])
    ) (TEMP tret)
translateExpression vars (ENot e)      = do
  me <- translateExpression vars e
  return $ BINOP XOR me (CONST 1)
translateExpression vars (EBraces e)   = translateExpression vars e
translateConditional _ (ELessThan (ENum i) (ENum j)) l r = do
  if (i < j)
    then return $ jump l 
    else return $ jump r
translateConditional vars e l r = do
  (op,me1,me2) <- translateCondition e
  return $ CJUMP op me1 me2 l r
  where
    translateCondition (ENot e) = do
      (op,me1,me2) <- translateCondition e
      return (neg op,me1,me2)
    translateCondition (ELessThan e1 e2) = do
      me1 <- translateExpression vars e1
      me2 <- translateExpression vars e2
      return (LT, me1, me2)
    translateCondition e = do
      me <- translateExpression vars e
      return (EQ,me,CONST 1)

translateStatement
  :: (Functor m, MachineSpecifics m a f) =>
       Map String (Exp, Type) -> Statement -> ReaderT ProgramTable m Stm
translateStatement vars (SIf econd sif selse) = do
  lif    <- nextLabel
  lelse  <- nextLabel
  lexit  <- nextLabel
  mecond <- translateConditional vars econd lif lelse
  msif   <- translateStatement vars sif
  mselse <- translateStatement vars selse
  return $ toSeq $ mecond
    : LABEL lif   : msif   : jump lexit
    : LABEL lelse : mselse : LABEL lexit : []
translateStatement vars (SScope xs) = fmap toSeq $ mapM (translateStatement vars) xs
translateStatement vars (SWhile cond body) = do
  lcstart <- nextLabel
  lbstart <- nextLabel
  lbexit  <- nextLabel
  mcond   <- translateConditional vars cond lbstart lbexit
  mbody   <- translateStatement vars body
  return $ toSeq $ LABEL lcstart : mcond
    : LABEL lbstart : mbody : jump lcstart
    : LABEL lbexit : []
translateStatement vars (SPrintLn e) = do
  me <- translateExpression vars e
  return $ EXP $ CALL (NAME "L_println_int") [me]
translateStatement vars (SPrint e) = do
  me <- translateExpression vars e
  return $ EXP $ CALL (NAME "L_print_char") [me]
translateStatement vars (SAssign s e) = do
  me  <- translateExpression vars e
  return $ MOVE (fst $ vars ! s) me
translateStatement vars (SArrayAssign s p e) = do
  mp   <- translateExpression vars p
  me   <- translateExpression vars e
  tvar <- nextTemp
  toff <- nextTemp
  tpos <- nextTemp
  size <- wordSize
  return $ toSeq $
    MOVE (TEMP tvar) (fst $ vars ! s) :
    MOVE (TEMP toff) (scale (plus (CONST 1) mp) size) :
    MOVE (TEMP tpos) (plus (TEMP tvar) (TEMP toff)) :
    MOVE (MEM $ TEMP tpos) (me) : []

translateMethod
  :: (Functor m, MachineSpecifics m a f) =>
       Class -> Method -> ReaderT ProgramTable m (Fragment f Stm)
translateMethod c m = do
  size <- wordSize
  frame <- mkFrame (className c ++ "$$" ++ methName m) (1 + (length $ methArgs m))
  (f, vars) <- allocLocals frame (methVars m)
  stms <- mapM (translateStatement (addVars size f vars)) $ methStms m
  rexp <- translateExpression (addVars size f vars) $ methRetExp m
  body <- makeProc f (toSeq stms) rexp
  return $ FragmentProc f body
  where
    allocLocals f []     = return (f,Map.empty)
    allocLocals f (x:xs) = do
      (f' ,t) <- allocLocal  f Anywhere
      (f'',m) <- allocLocals f' xs
      return (f'', Map.insert (varName x) (t,varType x) m)
    addVars size frame = (addClassVars size frame) . (addMethParams frame)
    addMethParams frame vars =
      Map.union vars $ Map.fromList $ zip ("this" : map varName (methArgs m))
        (zip (params frame) $ TClass (className c) : map varType (methArgs m))
    addClassVars size frame vars =
      Map.union vars $ classVarTable' (head $ params frame) (size) size (classVars c)
    classVarTable' _    _   _    []     = Map.empty
    classVarTable' this pos size (x:xs) = 
      Map.insert (varName x) 
                 (MEM $ plus (CONST pos) this,varType x)
                 (classVarTable' this (pos+size) size xs)

translateClass
  :: (Functor m, MachineSpecifics m a f) =>
       Class -> ReaderT ProgramTable m [Fragment f Stm]
translateClass c = 
  mapM (translateMethod c) (classMeths c)

translateMain
  :: (Functor m, MachineSpecifics m a f) =>
       MainClass -> ReaderT ProgramTable m (Fragment f Stm)
translateMain m = do
  f    <- mkFrame "Lmain" 1
  exp  <- translateStatement Map.empty $ mainClassStm m
  body <- makeProc f exp (CONST 0)
  return $ FragmentProc f body

translateProgram
  :: (Functor m, MachineSpecifics m a f) =>
       Program -> ReaderT ProgramTable m [Fragment f Stm]
translateProgram p = 
  liftM2 (:) (translateMain $ progMain p)
             (liftM concat $ mapM translateClass $ progClasses p)
