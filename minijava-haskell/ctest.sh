#!/bin/zsh

mkdir -p test/{Small,Large}

for i in ../MiniJava-Beispiele/{Small,Large}/*.java ; do
  TESTNAME=${${i/.java/}/..\/MiniJava-Beispiele\//}
  echo $TESTNAME
  [ -x ${i/.java/.class} ] || javac $i
  ./minijava $i | clang -xc - runtime64.c -o test/$TESTNAME -Wno-unused-value -Wno-implicit-function-declaration -Wno-format
#  [ $? -ne 0 ] && exit $?
  java -cp $(dirname $i) $(basename $TESTNAME) > test/$TESTNAME.javaout
  test/$TESTNAME > test/$TESTNAME.cout
  [ $? -ne 0 ] && { echo "execution failed" ; exit $? }
  if $(diff -ua test/$TESTNAME.cout test/$TESTNAME.javaout) ; then
    echo "ok"
  else
#    exit 1
  fi
done
