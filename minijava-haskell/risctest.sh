#!/bin/zsh

mkdir -p test/{Small,Large}

for i in ../MiniJava-Beispiele/{Small,Large}/*.java ; do
  TESTNAME=${${i/.java/}/..\/MiniJava-Beispiele\//}
  echo $TESTNAME
  [ -x ${i/.java/.class} ] || javac $i
  ./minijava $i | ./risc386 - 2> /dev/null > test/$TESTNAME.riscout
  [ $? -ne 0 ] && exit $?
  java -cp $(dirname $i) $(basename $TESTNAME) > test/$TESTNAME.javaout
  [ $? -ne 0 ] && { echo "execution failed" ; exit $? }
  if $(diff -ua test/$TESTNAME.riscout test/$TESTNAME.javaout) ; then
    echo "ok"
  else
    exit 1
  fi
done
