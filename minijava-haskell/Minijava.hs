module Minijava where
import Data.List

data Program
  = Program { progMain :: MainClass
            , progClasses :: [Class]
	    } deriving (Eq, Show)

data MainClass
  = MainClass { mainClassName :: String
              , mainClassArgv :: String
	      , mainClassStm :: Statement
	      } deriving (Eq, Show)

data Class
  = Class { className :: String
          , classSuper :: String
	  , classVars :: [Var]
	  , classMeths :: [Method]
	  } deriving (Eq, Show)

data Var
  = Var { varType :: Type
        , varName :: String
	} deriving (Eq, Show)

data Method
  = Method { methType :: Type
           , methName :: String
	   , methArgs :: [Var]
	   , methVars :: [Var]
	   , methStms :: [Statement]
	   , methRetExp :: Expression
	   } deriving (Eq, Show)

data Type
  = TIntArray
  | TBool
  | TInt
  | TClass String
  deriving (Eq, Show)

data Statement
  = SScope [Statement]
  | SIf Expression Statement Statement
  | SWhile Expression Statement
  | SPrintLn Expression
  | SPrint Expression
  | SAssign String Expression
  | SArrayAssign String Expression Expression
  deriving (Eq, Show)

data Expression
  = ELogicalAnd Expression Expression
  | ELessThan Expression Expression
  | EPlus Expression Expression
  | EMinus Expression Expression
  | ETimes Expression Expression
  | EDiv Expression Expression
  | EArrayAccess Expression Expression
  | ELength Expression
  | EFunc Expression String [Expression]
  | ENum Int
  | ETrue
  | EFalse
  | EIdent String
  | EThis
  | ENewArray Expression
  | ENew String
  | ENot Expression
  | EBraces Expression
  deriving (Eq, Show)

printIndent :: Int -> String
printIndent 0 = []
printIndent i = '\t':(printIndent (i-1))

printType :: Type -> String
printType  TIntArray = "int[]"
printType  TBool     = "boolean"
printType  TInt      = "int"
printType (TClass s) = s

printExpression :: Expression -> String
printExpression (ELogicalAnd e1 e2)
  = printExpression e1 ++ " && " ++ printExpression e2
printExpression (ELessThan e1 e2)
  = printExpression e1 ++ " < " ++ printExpression e2
printExpression (EPlus e1 e2)
  = printExpression e1 ++ " + " ++ printExpression e2
printExpression (EMinus e1 e2)
  = printExpression e1 ++ " - " ++ printExpression e2
printExpression (ETimes e1 e2)
  = printExpression e1 ++ " * " ++ printExpression e2
printExpression (EDiv e1 e2)
  = printExpression e1 ++ " / " ++ printExpression e2
printExpression (EArrayAccess e1 e2)
  = printExpression e1 ++ "[" ++ printExpression e2 ++ "]"
printExpression (ELength e)
  = printExpression e ++ ".length"
printExpression (EFunc e fn args)
  = printExpression e ++ "." ++ fn ++ "(" 
      ++ intercalate ", " (map printExpression args) ++ ")"
printExpression (ENum i)      = show i
printExpression  ETrue        = "true"
printExpression  EFalse       = "false"
printExpression (EIdent s)    = s
printExpression  EThis        = "this" 
printExpression (ENewArray e) = "new int [" ++ printExpression e ++ "]"
printExpression (ENew s)      = "new " ++ s ++ "()"
printExpression (ENot e)      = '!' : (printExpression e)
printExpression (EBraces e)   = '(' : (printExpression e) ++ ")" 

printStatement :: Int -> Statement -> String
printStatement i (SScope stms)
  = printIndent i ++ "{\n" ++ concat (map (printStatement (i+1)) stms) 
      ++ printIndent i ++ "}\n"
printStatement i (SIf cond eif eelse)
  = printIndent i ++ "if (" ++ printExpression cond ++ ")\n"
      ++ printStatement (i+1) eif ++ printIndent i ++ "else\n"
      ++ printStatement (i+1) eelse
printStatement i (SWhile cond body)
  = printIndent i ++ "while (" ++ printExpression cond ++ ")\n"
      ++ printStatement (i+1) body
printStatement i (SPrintLn e)
  = printIndent i ++ "System.out.println (" ++ printExpression e ++ ");\n"
printStatement i (SPrint e)
  = printIndent i ++ "System.out.print ((char)" ++ printExpression e ++ ");\n"
printStatement i (SAssign l r)
  = printIndent i ++ l ++ " = " ++ printExpression r ++ ";\n"
printStatement i (SArrayAssign n p e)
  = printIndent i ++ n ++ "[" ++ printExpression p ++ "] = " 
      ++ printExpression e ++ ";\n"

printVarDecl :: Int -> Var -> String
printVarDecl i (Var t name) =
  printIndent i ++ printType t ++ " " ++ name ++ ";\n"

printMethodArgs :: [Var] -> String
printMethodArgs []     = []
printMethodArgs [v]    = printType (varType v) ++ " " ++ varName v
printMethodArgs (v:vs) =
  printType (varType v) ++ " " ++ varName v ++ ", " ++ printMethodArgs vs

printMethodDecl :: Int -> Method -> String
printMethodDecl i (Method t n args vars stms rexp)
  = printIndent i ++ "public " ++ printType t ++ " " ++ n 
      ++ " (" ++ printMethodArgs args ++ ")\n"
      ++ printIndent i ++ "{\n"
      ++ (concat $ map (printVarDecl (i+1)) vars)
      ++ (concat $ map (printStatement (i+1)) stms) 
      ++ printIndent (i+1) ++ "return " ++ printExpression rexp ++ ";\n"
      ++ printIndent i ++ "}\n"

printExtends :: String -> String
printExtends "" = ""
printExtends e  = " extends " ++ e

printClass :: Class -> String
printClass (Class name super vars methods)
  = "class " ++ name ++ printExtends super ++ "\n"
      ++ "{\n"
      ++ (concat $ map (printVarDecl 1) vars)
      ++ (concat $ map (printMethodDecl 1) methods)
      ++ "}\n"

printMain :: MainClass -> String
printMain (MainClass name argv stm)
  = "\nclass " ++ name ++ "\n"
      ++ "{\n"
      ++ printIndent 1 ++ "public static void main (String[] " ++ argv ++ ")\n"
      ++ printIndent 1 ++ "{\n" 
      ++ printStatement 2 stm 
      ++ printIndent 1 ++ "}\n"
      ++ "}\n"

printProgram :: Program -> String
printProgram (Program main classes)
  = printMain main ++ (concat $ map printClass classes)
