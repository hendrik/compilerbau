#!/bin/zsh

mkdir -p test/{Small,Large}

for i in ../MiniJava-Beispiele/{Small,Large}/*.java ; do
  TESTNAME=${${i/.java/}/..\/MiniJava-Beispiele\//}
  echo $TESTNAME
  [ -x ${i/.java/.class} ] || javac $i
  ./minijava $i | gcc -m32 -xassembler - -xc runtime.c -o test/$TESTNAME
  test/$TESTNAME > test/$TESTNAME.nativeout
  [ $? -ne 0 ] && exit $?
  java -cp $(dirname $i) $(basename $TESTNAME) > test/$TESTNAME.javaout
  [ $? -ne 0 ] && { echo "execution failed" ; exit $? }
  if $(diff -ua test/$TESTNAME.nativeout test/$TESTNAME.javaout) ; then
    echo "ok"
  else
    exit 1
  fi
done
