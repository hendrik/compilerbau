{
module Scanner where
}

%wrapper "basic"

$digit      = 0-9
$graphic    = [_a-zA-Z0-9]

tokens :-

  "/*".*"*/"           ;
  "//".*$              ;
  $white+              ;


  "!"                  { \s -> BANG }
  "&&"                 { \s -> ANDAND }
  "("                  { \s -> LPAREN }
  ")"                  { \s -> RPAREN }
  "*"                  { \s -> ASTERISK }
  "+"                  { \s -> PLUS }
  ","                  { \s -> COMMA }
  "-"                  { \s -> MINUS }
  "."                  { \s -> DOT }
  "/"                  { \s -> SLASH }
  ";"                  { \s -> SEMICOLON }
  "<"                  { \s -> LESSTHAN }
  "="                  { \s -> EQUALS }
  "["                  { \s -> LBRACKETS }
  "]"                  { \s -> RBRACKETS }
  "{"                  { \s -> LBRACES }
  "}"                  { \s -> RBRACES }

  "System.out.print"   { \s -> PRINT }
  "System.out.println" { \s -> PRINTLN }
  "boolean"            { \s -> BOOLEAN }
  "char"               { \s -> CHAR }
  "class"              { \s -> CLASS }
  "else"               { \s -> ELSE }
  "extends"            { \s -> EXTENDS }
  "false"              { \s -> FALSE }
  "if"                 { \s -> IF }
  "int"                { \s -> INT }
  "length"             { \s -> LENGTH }
  "main"               { \s -> MAIN }
  "new"                { \s -> NEW }
  "public"             { \s -> PUBLIC }
  "return"             { \s -> RETURN }
  "static"             { \s -> STATIC }
  "this"               { \s -> THIS }
  "true"               { \s -> TRUE }
  "void"               { \s -> VOID }
  "while"              { \s -> WHILE }

  $digit+              { \s -> INTEGER (read s) }
  $graphic+            { \s -> IDENTIFIER s }
{

data Token = Bang
  | ANDAND
  | ASTERISK
  | BANG
  | BOOLEAN
  | CHAR
  | CLASS
  | COMMA
  | DOT
  | ELSE
  | EOF
  | EQUALS
  | EXTENDS
  | FALSE
  | IDENTIFIER String
  | IF
  | INT
  | INTEGER Int
  | LBRACES
  | LBRACKETS
  | LENGTH
  | LESSTHAN
  | LPAREN
  | MAIN
  | MINUS
  | NEW
  | PLUS
  | PRINT
  | PRINTLN
  | PUBLIC
  | RBRACES
  | RBRACKETS
  | RETURN
  | RPAREN
  | SEMICOLON
  | SLASH
  | STATIC
  | THIS
  | TRUE
  | VOID
  | WHILE
  deriving (Eq, Show)

}
