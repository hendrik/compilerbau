module Symbols where
import Minijava
import Data.Map (Map)
import qualified Data.Map as Map

data MethodTable
  = MethodTable { methTName :: String
                , methTType :: Type
		, methTArgs :: [(String,Type)]
		, methTVars :: Map String Type
		} deriving (Show)

data ClassTable
  = ClassTable { classTName :: String
               , classTSize :: Int
               , classTSuper :: String
	       , classTVars :: Map String Type
	       , classTMeths :: Map String MethodTable
	       } deriving (Show)

data ProgramTable
  = ProgramTable { progTClasses :: Map String ClassTable
                 } deriving (Show)

-- inserts only new elements, error if element allready exists
insertNew :: Ord k => k -> a -> Map k a -> Map k a
insertNew k x m =
  if Map.member k m
    then error "Element already exists in symbol table"
    else Map.insert k x m

genVarSymbols :: [Var] -> (Map String Type)
genVarSymbols [] = Map.empty
genVarSymbols ((Var t s):xs) = Map.insert s t $ genVarSymbols xs

genMethodSymbolTable :: Method -> MethodTable
genMethodSymbolTable (Method rtype name args vars stms rexp) =
  MethodTable name rtype (zip (map varName args) (map varType args))
    (genVarSymbols vars)
  where
    swap (a,b) = (b,a)

genClassSymbolTable :: Class -> ClassTable
genClassSymbolTable (Class name super vars methods) =
  ClassTable name size super
    (Map.insert "this" (TClass name) $ genVarSymbols vars)
    (genMethodSymbols methods)
  where
    genMethodSymbols :: [Method] -> (Map String MethodTable)
    genMethodSymbols []     = Map.empty
    genMethodSymbols (m:ms) = 
      Map.insert (methName m) (genMethodSymbolTable m) (genMethodSymbols ms)
    size :: Int
    size = length vars + 1

genSymbolTable :: Program -> ProgramTable
genSymbolTable (Program main classes) = 
  ProgramTable (genClassSymbols classes)
  where
    genClassSymbols :: [Class] -> (Map String ClassTable)
    genClassSymbols []     = Map.empty
    genClassSymbols (c:cs) = 
      Map.insert (className c) (genClassSymbolTable c) (genClassSymbols cs)
