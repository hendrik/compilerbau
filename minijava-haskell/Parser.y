{
module Parser where
import Scanner
import Minijava
}

%name parser
%tokentype { Token }
%error { parseError }

%token
  "!"                  { BANG }
  "&&"                 { ANDAND }
  "("                  { LPAREN }
  ")"                  { RPAREN }
  "*"                  { ASTERISK }
  "+"                  { PLUS }
  ","                  { COMMA }
  "-"                  { MINUS }
  "."                  { DOT }
  "/"                  { SLASH }
  ";"                  { SEMICOLON }
  "<"                  { LESSTHAN }
  "="                  { EQUALS }
  "["                  { LBRACKETS }
  "]"                  { RBRACKETS }
  "{"                  { LBRACES }
  "}"                  { RBRACES }
  "System.out.print"   { PRINT }
  "System.out.println" { PRINTLN }
  "boolean"            { BOOLEAN }
  "char"               { CHAR }
  "class"              { CLASS }
  "else"               { ELSE }
  "extends"            { EXTENDS }
  "false"              { FALSE }
  "if"                 { IF }
  "int"                { INT }
  "length"             { LENGTH }
  "main"               { MAIN }
  "new"                { NEW }
  "public"             { PUBLIC }
  "return"             { RETURN }
  "static"             { STATIC }
  "this"               { THIS }
  "true"               { TRUE }
  "void"               { VOID }
  "while"              { WHILE }
  Integer              { INTEGER $$ }
  Identifier           { IDENTIFIER $$ }

%left "&&"
%nonassoc "<"
%left "-" "+"
%left "*" "/"
%right "!"
%left "." "["

%%

Program
 : MainClass ClassDeclList { Program $1 $2 }

MainClass
 : "class" Identifier "{" "public" "static" "void" "main" "(" Identifier
   "[" "]" Identifier ")" "{" Statement "}" "}" 
   { if $9 == "String" 
       then MainClass $2 $12 $15
       else error "main class argument is not of type String" }

ClassDeclList
 : {- nothing -}           { [] }
 | ClassDeclList ClassDecl { $1 ++ [$2] }

ClassDecl
 : "class" Identifier Extends "{" VarDeclList MethodDeclList "}"
   { Class $2 $3 $5 $6 }

Extends
 : {- nothing -}        { "" }
 | "extends" Identifier { $2 }

VarDeclList
 : {- nothing -}       { [] }
 | VarDeclList VarDecl { $1 ++ [$2] }

VarDecl
 : Type Identifier ";" { Var $1 $2 }

MethodDeclList
 : {- nothing -}             { [] }
 | MethodDeclList MethodDecl { $1 ++ [$2] }

MethodDecl
 : "public" Type Identifier "(" MethodArgumentList ")" "{"
   VarsThenStatements "return" Expression ";" "}" 
   { Method $2 $3 $5 (fst $8) (snd $8) $10 }

MethodArgumentList
 : {- nothing -}         { [] }
 | MethodArgumentSubList { $1 }

MethodArgumentSubList
 : Type Identifier                           { [Var $1 $2] }
 | MethodArgumentSubList "," Type Identifier { $1 ++ [Var $3 $4] }

VarsThenStatements
 : VarDecl VarsThenStatements { ($1:(fst $2), (snd $2)) }
 | Statement StatementList    { ([],$1:$2) }
 | {- nothing -}              { ([],[]) }

Type
 : "int" "[" "]" { TIntArray }
 | "boolean"     { TBool }
 | "int"         { TInt }
 | Identifier    { TClass $1 }

StatementList
 : {- nothing -}           { [] }
 | StatementList Statement { $1 ++ [$2] }

Statement
 : "{" StatementList "}" { SScope $2 }
 | "if" "(" Expression ")" Statement "else" Statement { SIf $3 $5 $7 }
 | "while" "(" Expression ")" Statement { SWhile $3 $5 }
 | "System.out.println" "(" Expression ")" ";" { SPrintLn $3 }
 | "System.out.print" "(" "(" "char" ")" Expression ")" ";" { SPrint $6 }
 | Identifier "=" Expression ";" { SAssign $1 $3 }
 | Identifier "[" Expression "]" "=" Expression ";" { SArrayAssign $1 $3 $6 }

Expression
 : Expression "&&" Expression { ELogicalAnd $1 $3 }
 | Expression "<"  Expression { ELessThan $1 $3 }
 | Expression "+"  Expression { EPlus $1 $3 }
 | Expression "-"  Expression { EMinus $1 $3 }
 | Expression "*"  Expression { ETimes $1 $3 }
 | Expression "/"  Expression { EDiv $1 $3 }
 | Expression "["  Expression "]" { EArrayAccess $1 $3 }
 | Expression "." "length" { ELength $1 }
 | Expression "." Identifier "(" MethodParameterList ")"
   { EFunc $1 $3 $5 }
 | "!" Expression { ENot $2 }
 | Integer { ENum $1 }
 | "true" { ETrue }
 | "false" { EFalse }
 | Identifier { EIdent $1 }
 | "this" { EThis }
 | "new" "int" "[" Expression "]" { ENewArray $4 }
 | "new" Identifier "(" ")" { ENew $2 }
 | "(" Expression ")" { EBraces $2 }

MethodParameterList
 : {- nothing -} { [] }
 | MethodParameterSubList { $1 }

MethodParameterSubList
 : Expression { [$1] }
 | MethodParameterSubList "," Expression { $1 ++ [$3] }

{

parseError :: [Token] -> a
parseError e = error $ "Parser error\nRemaining tokens: " ++ show e

}
