module Canonizer (
  canonize
) where

import Tree
import MachineSpecifics (Fragment(..))

canonizeExp :: Exp -> ([Stm], Exp)
canonizeExp e@(CONST _) = ([], e)
canonizeExp e@(NAME _)  = ([], e)
canonizeExp e@(TEMP _)  = ([], e)
canonizeExp (MEM exp) =
  let (sexp, eexp) = canonizeExp exp in
  (sexp, MEM eexp)
canonizeExp (BINOP op left right) =
  let (sleft,  eleft)  = canonizeExp left in
  let (sright, eright) = canonizeExp right in
  (sleft ++ sright, BINOP op eleft eright)
canonizeExp (CALL func args) =
  let (sfunc, efunc) = canonizeExp func in
  let (sargs, eargs) = unzip (map canonizeExp args) in
  (sfunc ++ concat sargs, CALL efunc eargs)
canonizeExp (ESEQ stm exp) =
  let (sexp, eexp) = canonizeExp exp in
  (canonizeStm stm ++ sexp, eexp)

canonizeStm :: Stm -> [Stm]
canonizeStm (MOVE (TEMP t) call@(CALL _ _)) =
  let (scall, ecall) = canonizeExp call in
  scall ++ [MOVE (TEMP t) ecall]
canonizeStm (MOVE dest src) = 
  let (ds, de) = canonizeExp dest in
  let (ss, se) = canonizeExp src in
  ds ++ ss ++ [MOVE (de) (se)]
canonizeStm (EXP e) =
  let (es, ee) = canonizeExp e in
  es ++ [EXP ee]
canonizeStm (JUMP dest poss) =
  let (dests, deste) = canonizeExp dest in
  dests ++ [JUMP deste poss]
canonizeStm (CJUMP op left right true false) =
  let (sleft,  eleft)  = canonizeExp left in
  let (sright, eright) = canonizeExp right in
  sleft ++ sright ++ [CJUMP op eleft eright true false]
canonizeStm (SEQ f s) = canonizeStm f ++ canonizeStm s
canonizeStm l@(LABEL _) = [l]
canonizeStm NOP = []

canonizeFragment :: Fragment f Stm -> Fragment f [Stm]
canonizeFragment (FragmentProc f body) =
  FragmentProc f (canonizeStm body)

canonize :: [Fragment f Stm] -> [Fragment f [Stm]]
canonize = map canonizeFragment
