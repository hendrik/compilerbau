module Main where

import Data.Maybe
import Canonizer
import Cmm
import Control.Monad
import Control.Monad.Trans
import Control.Monad.Trans.Reader
import Data.Functor.Identity
import Minijava
import Names
import Optimizer
import Parser
import Scanner
import Symbols
import TreeTranslator
import TypeChecker
import MachineSpecifics
import Blocks
import X86Machine
import Allocator
import DummyMachine
import Data.GraphViz
import Data.Graph.Inductive.Graphviz
import Data.Graph.Inductive.Graph
import System.Environment

main :: IO ()
main = do
  args <- getArgs
  if null args
    then error "Usage: minijava <source file>"
    else do
      inStr <- if head args == "-" 
                 then getContents 
                 else readFile $ head args
      let parseTree = parser (alexScanTokens inStr) 
      let symbols = genSymbolTable parseTree
      case checkProgram parseTree symbols of
        Nothing -> withX86Machine $ do
          prog   <- runReaderT (translateProgram parseTree) symbols
          reordered <- mapM reorderFragment $ canonize prog
          output <- mapM codeGen reordered
	  regs <- getRegs
	  k <- getK
--	  lift $ putStrLn $ graphviz' $ snd $ curry (simplify regs k) [] $ buildInterference $ body $ last output
--	  lift $ putStrLn $ show $ fst $ curry (simplify regs k) [] $ buildInterference $ body $ last output
--	  lift $ putStrLn $ graphviz' $ buildInOut $ buildTree $ body $ last output
--


	  
          allocd <- allocateRegs output
--	  lift $ putStrLn $ show allocd
	  corrected <- mapM adjustEsp allocd
          string <- printAssembly corrected
          lift $ putStrLn string
          {-
        Nothing -> withDummyMachine $ do
--          lift $ putStrLn $ show $ parseTree
          prog   <- runReaderT (translateProgram parseTree) symbols
--          mapM (lift . putStrLn . show . body) (canonize prog)
--          output <- cmmDoc prog
--          lift $ putStrLn $ show output
--          lift $ putStrLn ""
          prog <- mapM reorderFragment (canonize prog)
          output <- cmmsDoc prog
          lift $ putStrLn $ show $ output
	  -}
        Just s  -> error $ unlines $ "Type error:" : s
  where
    getRegs = do
      regs <- generalPurposeRegisters
      return $ fromMaybe [] regs
    getK = do
      regs <- generalPurposeRegisters
      return $ maybe 0 length regs
