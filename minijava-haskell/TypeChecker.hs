module TypeChecker where
import Minijava
import Symbols
import Data.Monoid
import Data.Map ((!))
import qualified Data.Map as Map

typeOf :: ProgramTable -> ClassTable -> Expression -> Type
typeOf _ _ (ELogicalAnd _ _)  = TBool
typeOf _ _ (ELessThan _ _)    = TBool
typeOf _ _ (EPlus _ _)        = TInt
typeOf _ _ (EMinus _ _)       = TInt
typeOf _ _ (ETimes _ _)       = TInt
typeOf _ _ (EDiv _ _)         = TInt
typeOf _ _ (EArrayAccess _ _) = TInt
typeOf _ _ (ELength _)        = TInt
typeOf p c (EFunc a f _)      = 
  case typeOf p c a of
    (TClass s) -> methTType ((classTMeths (progTClasses p ! s)) ! f)
    _ -> error "BUGBUG: function on non class after typechecker"
typeOf _ _ (ENum _)           = TInt
typeOf _ _ ETrue              = TBool
typeOf _ _ EFalse             = TBool
typeOf _ _ (EIdent s)         = TClass s
typeOf _ c EThis              = TClass $ classTName c
typeOf _ _ (ENewArray _)      = TIntArray
typeOf _ _ (ENew s)           = TClass s 
typeOf _ _ (ENot _)           = TBool
typeOf p c (EBraces e)        = typeOf p c e

checkForClass :: ProgramTable -> String -> Maybe [String]
checkForClass p s =
  case Map.member s (progTClasses p) of
    True  -> Nothing
    False -> (Just ["undefined class name: " ++ s])

checkValidType :: ProgramTable -> Type -> Maybe [String]
checkValidType p e =
  case e of
    TInt       -> Nothing
    TBool      -> Nothing
    TIntArray  -> Nothing
    (TClass s) -> checkForClass p s

maybeToEither :: Type -> Maybe [String] -> Either Type [String]
maybeToEither t Nothing  = Left t
maybeToEither _ (Just s) = Right s

checkVar :: ClassTable -> MethodTable -> String -> Either Type [String]
checkVar c m s =
  case Map.lookup s (methTVars m) of
    Just t  -> Left t
    Nothing ->
      case lookup s (methTArgs m) of
        Just t  -> Left t
        Nothing ->
          case Map.lookup s (classTVars c) of
            Just t  -> Left t
            Nothing -> Right ["unknown variable " ++ s]

checkBinary :: Type -> ProgramTable -> ClassTable -> MethodTable
  -> Expression -> Expression -> Either Type [String]
checkBinary t p c m e1 e2 =
  maybeToEither t $ mappend (checkExprType p c m t e1)
                                (checkExprType p c m t e2)

checkExpression :: ProgramTable -> ClassTable -> MethodTable -> Expression
  -> Either Type [String]
checkExpression p c m (ELogicalAnd e1 e2) =
  checkBinary TBool p c m e1 e2
checkExpression p c m (ELessThan e1 e2) =
  maybeToEither TBool $ mappend (checkExprType p c m TInt e1)
                                    (checkExprType p c m TInt e2)
checkExpression p c m (EPlus e1 e2) =
  checkBinary TInt p c m e1 e2
checkExpression p c m (EMinus e1 e2) =
  checkBinary TInt p c m e1 e2
checkExpression p c m (ETimes e1 e2) =
  checkBinary TInt p c m e1 e2
checkExpression p c m (EDiv e1 e2) =
  checkBinary TInt p c m e1 e2
checkExpression p c m (EArrayAccess e1 e2) =
  maybeToEither TInt $ mappend (checkExprType p c m TIntArray e1)
                                   (checkExprType p c m TInt e2)
checkExpression p c m (ELength e) =
  maybeToEither TInt $ checkExprType p c m TIntArray e
checkExpression p c m (EFunc t n ps) =
  case method of
    Right s -> Right s
    Left mt -> maybeToEither (methTType mt) $ mconcat $
      (if not (length (methTArgs mt) == length ps)
         then Just ["number of function arguments does not match"]
	 else Nothing) :
      (zipWith (checkExprType p c m) (map snd $ methTArgs mt) ps)
  where
    method :: Either MethodTable [String]
    method = case checkExpression p c m t of
               Right s -> Right s
               Left (TClass s) ->
                 case Map.lookup n (classTMeths (progTClasses p ! s)) of
                   Nothing -> Right ["method " ++ n ++ " unknown in class " ++ s]
                   Just e -> Left e
               Left l  -> Right ["method invocation on non class type " ++ show l]
checkExpression _ _ _ (ENum _) = Left TInt
checkExpression _ _ _ ETrue    = Left TBool
checkExpression _ _ _ EFalse   = Left TBool
checkExpression _ c m (EIdent s) = checkVar c m s
checkExpression _ c _ EThis = Left $ (classTVars c) ! "this"
checkExpression p c m (ENewArray e) =
  maybeToEither TIntArray $ checkExprType p c m TInt e
checkExpression p _ _ (ENew s) =
  case Map.lookup s (progTClasses p) of
    Nothing -> Right ["new of non existing class " ++ s]
    Just _  -> Left $ TClass s
checkExpression p c m (ENot e) =
  maybeToEither TBool $ checkExprType p c m TBool e
checkExpression p c m (EBraces e) =
  checkExpression p c m e

checkExprType :: ProgramTable -> ClassTable -> MethodTable -> Type -> Expression
  -> Maybe [String]
checkExprType p c m t e =
  case checkExpression p c m e of
    Left t2  -> if t == t2
                  then Nothing
                  else Just ["In " ++ classTName c ++ "." ++ methTName m ++
		              ": expression \"" ++ show e ++ "\" has wrong type: "
			      ++ show t2 ++ " instead of " ++ show t]
    Right xs -> Just xs

checkStatement :: ProgramTable -> ClassTable -> MethodTable -> Statement
  -> Maybe [String]
checkStatement p c m (SScope xs) =
  mconcat $ map (checkStatement p c m) xs
checkStatement p c m (SIf cond i e) =
  mconcat $
    checkExprType p c m TBool cond : checkStatement p c m i :
      checkStatement p c m e : []
checkStatement p c m (SWhile cond body) =
  mconcat $
    checkExprType p c m TBool cond : checkStatement p c m body : []
checkStatement p c m (SPrintLn e) =
  checkExprType p c m TInt e
checkStatement p c m (SPrint e) =
  checkExprType p c m TInt e
checkStatement p c m (SAssign s e) =
  case checkVar c m s of
    Left t  -> checkExprType p c m t e
    Right t -> Just t
checkStatement p c m (SArrayAssign s pos e) =
  mconcat $
    (case checkVar c m s of
      Left TIntArray -> Nothing
      Left t         -> Just ["array access on var of type " ++ show t]
      Right t        -> Just t) :
    checkExprType p c m TInt pos : checkExprType p c m TInt e : []

checkMethod :: ProgramTable -> ClassTable -> Method -> Maybe [String]
checkMethod p c m =
  case Map.lookup (methName m) (classTMeths c) of
    Nothing     -> Just ["Method " ++ methName m ++
                         " doesn't exist in class " ++ classTName c]
    Just mTable ->
      mconcat $
        checkValidType p (methType m)
         : checkExprType p c mTable (methType m) (methRetExp m)
         : map (checkValidType p . varType) (methArgs m)
        ++ map (checkValidType p . varType) (methVars m)
        ++ map (checkStatement p c mTable) (methStms m)

checkSuper :: ProgramTable -> Class -> Maybe [String]
checkSuper p c =
  case classSuper c of
    "" -> Nothing
    s  -> checkForClass p s

checkClass :: ProgramTable -> Class -> Maybe [String]
checkClass p c =
  case Map.lookup (className c) (progTClasses p) of
    Nothing     -> Just ["Class " ++ className c ++ " doesn't exist"]
    Just cTable ->
      mconcat $ checkSuper p c
        :  map (checkValidType p . varType) (classVars c)
        ++ map (checkMethod p cTable) (classMeths c)

checkMain :: ProgramTable -> MainClass -> Maybe [String]
checkMain p (MainClass name _ stm) =
  checkStatement p (ClassTable name 1 "" Map.empty Map.empty)
    (MethodTable "main" TInt [] Map.empty) stm

checkProgram :: Program -> ProgramTable -> Maybe [String]
checkProgram (Program m cs) p =
  mconcat $ checkMain p m : map (checkClass p) cs
