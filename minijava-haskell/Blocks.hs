module Blocks (
  Block(..),
  splitInBlocks,
  traceBlocks,
  reorderFragment
) where

import Names
import Tree
import MachineSpecifics (Fragment(..))
import Data.Map (Map)
import qualified Data.Map as Map

data Block = Block { blockStart :: Stm
                   , blockStms  :: [Stm]
                   , blockEnd   :: Stm
                   } deriving Show

findEndBlock :: Label -> Stm -> [Stm] -> [Stm] -> [Block]
findEndBlock lend l bstms stms@((LABEL lnext):_) =
  Block l bstms (jump lnext) : findStartBlock lend stms
findEndBlock lend l bstms (jump@(JUMP _ _):stms) =
  Block l bstms jump : findStartBlock lend stms
findEndBlock lend l bstms (jump@(CJUMP _ _ _ _ _):stms) =
  Block l bstms jump : findStartBlock lend stms
findEndBlock lend l bstms (stm:stms) =
  findEndBlock lend l (bstms ++ [stm]) stms
findEndBlock lend l bstms [] = [Block l bstms (jump lend)]

findStartBlock :: Label -> [Stm] -> [Block]
findStartBlock lend (l@(LABEL _):stms) =
  findEndBlock lend l [] stms
-- dead code elimination
findStartBlock lend (_:stms) = findStartBlock lend stms

splitInBlocks :: MonadNameGen m => [Stm] -> m ([Block],Label)
splitInBlocks stms@((LABEL _):_) = do
  lend   <- nextLabel
  return $ (findStartBlock lend stms,lend)
splitInBlocks stms = do
  lbegin <- nextLabel
  lend   <- nextLabel
  return $ (findStartBlock lend ((LABEL lbegin) : stms),lend)

blockToStms :: Block -> [Stm]
blockToStms block = blockStart block : blockStms block ++ [blockEnd block]

-- Reorders the blocks in an optimized order.
-- Tries to place the corresponding block after a JUMP and the block of
-- the else path in case of an CJUMP
-- Automagicaly removes all unreachable blocks
traceBlocks :: ([Block],Label) -> [Stm]
traceBlocks (blocks,lend) =
  traceBlocks' [startLabel] blockMap ++ [LABEL lend]
  where
    traceBlocks' :: [Label] -> Map Label Block -> [Stm]
    traceBlocks' (l:ls) blocks =
      case Map.lookup l blocks of
      Just block -> blockToStms block ++
        case blockEnd block of
        (JUMP _ (nextl:_)) ->
          traceBlocks' (nextl:ls) (Map.delete l blocks)
        (CJUMP _ _ _ lif lelse) ->
          traceBlocks' (lelse:lif:ls) (Map.delete l blocks)
      Nothing -> 
        traceBlocks' ls blocks
    traceBlocks' [] _ = []
    blockMap :: Map Label Block
    blockMap = Map.fromList $ zip (map (label . blockStart) blocks) blocks
    startLabel :: Label
    startLabel =
      case blockStart $ head blocks of
        (LABEL l) -> l

reorderFragment :: MonadNameGen m => Fragment f [Stm] -> m (Fragment f [Stm])
reorderFragment (FragmentProc frame body) = do
  blocks <- splitInBlocks body
  return $ FragmentProc frame $ traceBlocks blocks
