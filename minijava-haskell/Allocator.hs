{-# LANGUAGE NoMonomorphismRestriction #-}
module Allocator where

import MachineSpecifics
import Names (Temp)
import Data.Graph.Inductive.Graph
import Data.Graph.Inductive.PatriciaTree
import Data.Set (Set)
import qualified Data.Set as Set
import qualified Data.List as List
import Data.Map (Map)
import qualified Data.Map as Map
import Data.Maybe
import Data.Tuple

(!) :: (Ord k, Show k, Show a) => Map k a -> k -> a
(!) map value =
  case Map.lookup value map of
    Just a  -> a
    Nothing -> error $ "element " ++ show value ++ " not in map.\n" ++ show map


data AsmNode a = AsmNode { asmCode :: a
                         , inVars  :: Set Temp
                         , outVars :: Set Temp
                         } deriving (Show)

buildInterference :: Assem a => [a] -> Gr Temp ()
buildInterference asms = findInterference $ buildInOut $ buildTree asms

buildTree :: Assem a => [a] -> Gr a ()
buildTree asms = uncurry mkGraph (genNodesEdges asms)
  where
    genNodesEdges asms = concatEdges $ unzip $ map genNodeEdge numberedAsms
    concatEdges (a,bs) = (a, concat bs)
    genNodeEdge (asm,pos) = ((pos,asm),findEdges asm pos)
    findEdges asm num = (fallEdge asm num) ++ (jumpEdge asm num)
    fallEdge asm num = if isFallThrough asm then [(num,num + 1,())] else []
    jumpEdge asm num = map (\s -> (num,s,())) (mapMaybe findJumpTarget (jumps asm))
    findJumpTarget target = List.lookup target labeledAsms
    numberedAsms = zip asms [1..]
    labeledAsms = concat $ map getLabel numberedAsms
    getLabel (asm,pos) = case isLabel asm of
                           Just l  -> [(l,pos)]
                           Nothing -> []

buildInOut :: Assem a => Gr a () -> Gr (AsmNode a) ()
buildInOut graph = recurse $ nmap switchNode graph
  where
    switchNode a = AsmNode a Set.empty Set.empty
    recurse :: (Assem a, Graph gr, DynGraph gr) => gr (AsmNode a) () -> gr (AsmNode a) ()
    recurse g = let g' = step g in 
      if cmp g g' then g else recurse g'
    cmp :: (Assem a, Graph gr) => gr (AsmNode a) () -> gr (AsmNode a) () -> Bool
    cmp g g' = 
      Set.fromList (map rmFst (labNodes g)) == (Set.fromList (map rmFst (labNodes g')))
    rmFst (n,asmnode) = (n,inVars asmnode, outVars asmnode)
    step :: (Assem a, Graph gr, DynGraph gr) => gr (AsmNode a) () -> gr (AsmNode a) ()
    step g = gmap (subStep g) g
    subStep :: (Assem a, Graph gr) =>
      gr (AsmNode a) () -> Context (AsmNode a) () -> Context (AsmNode a) ()
    subStep g n =
      let i' = Set.union (Set.fromList $ use $ asmCode $ lab' n) 
                         (Set.difference (outVars $ lab' n) 
                                         (Set.fromList $ def $ asmCode $ lab' n)) in
      let o' = Set.unions (map (inVars . fromJust . lab g) (suc' n)) in
      updateNode n i' o'
    updateNode (a,n,l,b) i' o' = (a,n,AsmNode (asmCode l) i' o',b)

findInterference :: (Assem a,Graph gr1,DynGraph gr1,Graph gr2) =>
  gr1 (AsmNode a) () -> gr2 Temp ()
findInterference g = mkGraph genNodes (Set.toList $ Set.fromList (ufold genEdges [] g))
  where
    nodeMap = Map.fromList $ map swap genNodes
    genNodes = zip [1..] (Set.foldr (:) [] $ ufold genNodes' Set.empty g)
    genNodes' c l = Set.unions (l : inVars (lab' c) : outVars (lab' c) : 
      Set.fromList (def $ asmCode $ lab' c) : 
      Set.fromList (use $ asmCode $ lab' c) : [])
    genEdges :: Assem a => Context (AsmNode a) () -> [(Node,Node,())] -> [(Node,Node,())]
    genEdges c l = case isMoveBetweenTemps $ asmCode $ lab' c of 
      Just (t, v) -> 
        map (\u -> (nodeMap ! t,nodeMap ! u,()))
            (Set.toList $ Set.delete v (outVars $ lab' c)) ++ l
      Nothing ->
        concatMap (\t -> map (\u -> (nodeMap ! t,nodeMap ! u,()))
                             (Set.toList $ outVars $ lab' c))
                  (def $ asmCode $ lab' c) ++ l

simplify :: Graph gr => [Temp] -> Int ->
  ([Context Temp ()],gr Temp ()) -> ([Context Temp ()],gr Temp ())
simplify aregs k (ss,gg) = recurse ss gg
  where
    recurse s g = let (s',g') = simplify' s g in
      if length s' == length s then (s',g') else recurse s' g'
    isColored n = n `elem` aregs
    simplify' s g =
      let (s',g') = splitNodes (s,g) $ ufold (findToRemove g k) [] g in
      (s',g')
    findToRemove g k c l =
      if not (isColored (lab' c)) && (deg g (node' c) < k) then node' c:l else l
    splitNodes (s,g) (n:ns) = case (match n g) of
      (mnode,g') -> splitNodes (fromJust mnode:s,g') ns
    splitNodes (s,g) [] = (s,g)

spillMax :: Graph gr => [Temp] -> Int ->
  ([Context Temp ()],gr Temp ()) -> ([Context Temp ()],gr Temp ())
spillMax aregs k (s,g) = let (mnode,g') = match (snd findMax) g in
    (fromJust mnode:s,g')
  where
    findMax = case ufold checkMax (0,0) g of
      (0,0) -> error "spillMax called, but nothing found"
      t -> t
    checkMax c (i,n) = 
      if (not (isColored (lab' c))) && ((deg g (node' c)) > i)
        then (deg g (node' c),node' c) else (i,n)
    isColored n = n `elem` aregs

selectRegisters :: (DynGraph gr,Graph gr) => [Temp] ->
  ([Context Temp ()],gr Temp ()) -> ([(Temp,Temp)],[Temp])
selectRegisters regs (sss,ggg) = select' (sss,ggg,[],[])
  where
    select' ((s:ss),g,rn,sp) = let g' =  s & g in
      case nextReg (node' s) g' of
        Just r -> select' (ss,renameNode s r & g,(lab' s,r):rn,sp)
	Nothing -> select' (ss,g',rn,lab' s:sp)
    select' ([],_,rn,sp) = (rn,sp)
    nextReg n g =
      let next = regs List.\\ (map (fromJust . lab g) (neighbors g n)) in
      if List.null next then Nothing else Just $ head next
    renameNode (a,n,_,b) r = (a,n,r,b)

--allocateRegsInFrame :: (Assem a, Frame f, MachineSpecifics m a f) =>
--  Fragment f [a] -> m (Fragment f [a])
allocateRegsInFrame frag = do
  regs <- getRegs
  aregs <- getAllRegs
  k <- getK
  f' <- recurse regs aregs k frag
  return f'
  where
--    recurse :: (Assem a, Frame f, MachineSpecifics m a f) =>
--      [Temp] -> [Temp] -> Int -> Fragment f [a] -> m (Fragment f [a])
    recurse regs aregs k f = do
      let g = buildInterference (body f)
      let (s',g') = recurseUntilColored aregs k ([],g)
      case selectRegisters regs (s',g') of
	-- no spill nodes
        (rn,[]) -> return $ renameNodes f rn
        -- spill and start again
        (rn,sp) -> do
	  f' <- runSpill (renameNodes f rn) sp
	  f'' <- recurse regs aregs k f'
	  return f''
    recurseUntilColored aregs k (s,g) = let (s',g') = simplify aregs k (s,g) in
      if graphIsColored aregs k g'
        then (s',g')
        else recurseUntilColored aregs k $ spillMax aregs k (s',g')
    graphIsColored aregs k g = ufold (\c b -> b && (isColored aregs c)) True g
    isColored aregs n = (lab' n) `elem` aregs
    renameNodes :: (Assem a, Frame f) =>
      Fragment f [a] -> [(Temp,Temp)] -> Fragment f [a]
    renameNodes f rn = FragmentProc (frame f) 
        $ map (\a -> foldl (\a' (f,t) -> rename a' (\s -> if s == f then t else s)) a rn) (body f)
    runSpill :: (Assem a, Frame f, MachineSpecifics m a f) =>
      Fragment f [a] -> [Temp] -> m (Fragment f [a])
    runSpill f sp = do 
      spilled <- spill (frame f) (body f) sp
      return $ uncurry FragmentProc spilled
    getRegs = do
      regs <- generalPurposeRegisters
      return $ fromMaybe [] regs
    getAllRegs = do
      aregs <- allRegisters
      return $ fromMaybe [] aregs
    getK = do
      regs <- generalPurposeRegisters
      return $ maybe 0 length regs

--allocateRegs :: (Assem a, Frame f, MachineSpecifics m a f) =>
--  [Fragment f [a]] -> m [Fragment f [a]]
allocateRegs = mapM allocateRegsInFrame
