module Optimizer (
  optimizeExp, optimizeStm
  ) where

import Prelude hiding (EQ,GT,LT)

import Tree
import Data.Bits ((.&.),(.|.),xor,shiftL,shiftR,clearBit,bitSize)

collapseConstants :: Exp -> Exp
collapseConstants (BINOP PLUS    (CONST c1) (CONST c2)) = CONST (c1 + c2)
collapseConstants (BINOP MINUS   (CONST c1) (CONST c2)) = CONST (c1 - c2)
collapseConstants (BINOP MUL     (CONST c1) (CONST c2)) = CONST (c1 * c2)
collapseConstants (BINOP DIV     (CONST c1) (CONST c2)) = CONST (div c1 c2)
collapseConstants (BINOP AND     (CONST c1) (CONST c2)) = CONST (c1 .&. c2)
collapseConstants (BINOP OR      (CONST c1) (CONST c2)) = CONST (c1 .|. c2)
collapseConstants (BINOP XOR     (CONST c1) (CONST c2)) = CONST (xor c1 c2)
collapseConstants (BINOP LSHIFT  (CONST c1) (CONST c2)) = CONST (shiftL c1 c2)
collapseConstants (BINOP RSHIFT  (CONST c1) (CONST c2)) =
  CONST (clearBit (shiftR c1 c2) (bitSize c1))
collapseConstants (BINOP ARSHIFT (CONST c1) (CONST c2)) = CONST (shiftR c1 c2)
collapseConstants (ESEQ _ (CONST c))                    = CONST c
collapseConstants e = e

optimizeExp' :: Exp -> Exp
optimizeExp' = collapseConstants

optimizeExp :: Exp -> Exp
optimizeExp (BINOP o e1 e2) =
  optimizeExp' $ BINOP o (optimizeExp e1) (optimizeExp e2)
optimizeExp (MEM e) =
  optimizeExp' $ MEM $ optimizeExp e
optimizeExp (CALL f args) =
  optimizeExp' $ CALL (optimizeExp f) (map optimizeExp args)
optimizeExp (ESEQ s e) =
  optimizeExp' $ ESEQ (optimizeStm s) (optimizeExp e)
optimizeExp e = optimizeExp' e

evaluateCJump :: Stm -> Stm
evaluateCJump (CJUMP EQ (CONST c1) (CONST c2) tl fl)
  | c1 == c2  = jump tl
  | otherwise = jump fl
evaluateCJump (CJUMP NE (CONST c1) (CONST c2) tl fl)
  | not (c1 == c2) = jump tl
  | otherwise      = jump fl
evaluateCJump (CJUMP LT (CONST c1) (CONST c2) tl fl)
  | c1 < c2   = jump tl
  | otherwise = jump fl
evaluateCJump (CJUMP GT (CONST c1) (CONST c2) tl fl)
  | c1 > c2   = jump tl
  | otherwise = jump fl
evaluateCJump (CJUMP LE (CONST c1) (CONST c2) tl fl)
  | c1 <= c2  = jump tl
  | otherwise = jump fl
evaluateCJump (CJUMP GE (CONST c1) (CONST c2) tl fl)
  | c1 >= c2  = jump tl
  | otherwise = jump fl
  
evaluateCJump s = s


-- helper function which calls all optimization subfunctions
optimizeStm' :: Stm -> Stm
optimizeStm' = evaluateCJump

optimizeStm :: Stm -> Stm
optimizeStm (MOVE e1 e2) = 
  optimizeStm' $ MOVE (optimizeExp e1) (optimizeExp e2)
optimizeStm (EXP e)      =
  optimizeStm' $ EXP (optimizeExp e)
optimizeStm (JUMP e l)   =
  optimizeStm' $ JUMP (optimizeExp e) l
optimizeStm (CJUMP r e1 e2 l1 l2) =
  optimizeStm' $ CJUMP r (optimizeExp e1) (optimizeExp e2) l1 l2
optimizeStm (SEQ s1 s2)  =
  optimizeStm' $ SEQ (optimizeStm s1) (optimizeStm s2)
optimizeStm s            = optimizeStm' s
