open Core.Std

module type Prefix = sig
  val prefix : string
end

module type Unique = sig
  type t with sexp
  val make : unit -> t
  val named : string -> t
  val to_string : t -> string
end

module Make(P : Prefix) : Unique = struct
  type t = string with sexp
    
  let current = ref 0
      
  let make () =
    let v = !current in
    current := v + 1;
    P.prefix ^ "$" ^ string_of_int v

  let named n =
    n
    
  let to_string (s : t) =
    s
end

module Label = Make(struct let prefix = "L$" end)
module Temp = Make(struct let prefix = "T$" end)
