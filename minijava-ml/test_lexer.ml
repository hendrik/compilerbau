let parse (file_name: string) =
  let ic = open_in file_name in
  let lexbuf = Lexing.from_channel ic in
  let rec loop () =
    try
      if Lexer.main lexbuf != Parser.EOF then
        let start_pos = lexbuf.Lexing.lex_start_p in
        Printf.printf
          "Token at line %i, character %i.\n"
          (start_pos.Lexing.pos_lnum)
          (start_pos.Lexing.pos_cnum - start_pos.Lexing.pos_bol + 1);
        loop ()
      else
        ()
    with
    | Parsing.Parse_error ->
      let start_pos = lexbuf.Lexing.lex_start_p in
      let msg = Printf.sprintf
          "Parse error at line %i, character %i."
          (start_pos.Lexing.pos_lnum)
          (start_pos.Lexing.pos_cnum - start_pos.Lexing.pos_bol + 1) in
      failwith msg
  in
  loop ();
  close_in ic

let usage_msg = "Usage: test_lexer.byte file1.java [file2.java ...]\n"

let main =
  let no_argument = ref true in
  let print_ast file_name =
    parse file_name;
    no_argument := false in
  Arg.parse [] print_ast "test_lexer file1.java [file2.java ...]";
  if !no_argument then print_string usage_msg
