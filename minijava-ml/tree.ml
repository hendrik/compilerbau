open Core.Std

open Label

type tree_exp =
  | Const of int
  | Name of Label.t
  | Temp of Temp.t
  | Binop of tree_exp * binop * tree_exp
  | Mem of tree_exp
  | Call of tree_exp * tree_exp list
  | Eseq of tree_stm * tree_exp

and tree_stm =
  | Move of tree_exp * tree_exp
  | Exp of tree_exp
  | Jump of Label.t
  | Cjump of tree_exp * relop * tree_exp * Label.t * Label.t
  | Seq of tree_stm * tree_stm
  | Label of Label.t
  | Nop

and binop =
  | Plus | Minus | Times | Div
  | And | Or | Xor

and relop =
  | Eq | Ne | Lt | Gt | Le | Ge

with sexp

let tree_exp_to_string t = Sexp.to_string_hum (sexp_of_tree_exp t)
let tree_stm_to_string t = Sexp.to_string_hum (sexp_of_tree_stm t)
