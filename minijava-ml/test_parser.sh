#!/bin/sh
set -e

check() {
  msg=$1; shift
  if eval "$@" >/dev/null; then
    echo ok - $msg
  else 
    echo fail - $msg
  fi
}

for f in ../MiniJava-Beispiele/{Small,Large}/*.java; do
  check "parse $f" "./test_parser.byte $f 2>/dev/null"
done
for f in ../MiniJava-Beispiele/ShouldFail/ParseErrors/{BigMain,BrokenParse*,MissingThis}.java; do
  check "detect parse error in $f" "! ./test_parser.byte $f 2>/dev/null"
done
