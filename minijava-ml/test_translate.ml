open Core.Std

open Typechecker

let parse (file_name: string) =
  In_channel.with_file file_name ~f:(fun ic ->
  let lexbuf = Lexing.from_channel ic in
    try
      Parser.main_stm Lexer.main lexbuf;
    with
    | Parser.Error ->
      let start_pos = lexbuf.Lexing.lex_start_p in
      let msg = Printf.sprintf
          "Parse error at line %i, character %i."
          (start_pos.Lexing.pos_lnum)
          (start_pos.Lexing.pos_cnum - start_pos.Lexing.pos_bol + 1) in
      failwith msg)

let usage_msg = "Usage: test_translate.byte file1.java [file2.java ...]\n"

let main =
  let no_argument = ref true in
  let print_ast file_name = begin
    let s = parse file_name in
    no_argument := false;
    print_endline "parse tree:";
    print_endline (Syntax.prg_to_string s);
    print_endline "semantic checker:";
    (match check_prg s with
     | Ok _ -> print_endline "OK"
     | Error msg -> print_endline ("Error: " ^ msg); exit 1);
    List.iter (Translate.translate_prg s) ~f:(fun (_, tstm) ->
      print_endline (Tree.tree_stm_to_string tstm);
      print_endline "";
      List.iter (Canonicalize.canon tstm) ~f:(fun tstm ->
        print_endline (Tree.tree_stm_to_string tstm));
      print_endline "";
      List.iter (Trace.trace (Canonicalize.canon tstm)) ~f:(fun block ->
        print_endline "---";
        List.iter block ~f:(fun tstm ->
          print_endline (Tree.tree_stm_to_string tstm)));
      print_endline "canon:";
      List.iter (Trace.trace_and_reorder (Canonicalize.canon tstm))
        ~f:(fun tstm ->
          print_endline (Tree.tree_stm_to_string tstm));
      print_endline "";
      Cgen.dump (Trace.trace_and_reorder
                   (Canonicalize.canon tstm));
      print_endline "munched:";
      print_endline (String.concat ~sep:"\n"
                       (List.map ~f:Assem.to_string
                          (Maxmunch.munch
                             (Trace.trace_and_reorder
                                (Canonicalize.canon tstm)))));
    );
    ();
  end in
  Arg.parse [] print_ast "test_translate file1.java [file2.java ...]";
  if !no_argument then print_string usage_msg
