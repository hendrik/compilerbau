open Core.Std
module Map = Core.Core_map

open Syntax
open Tree
open Label
open Frame

module LocalTable = Map.Make (String)
type localtable = tree_exp LocalTable.t with sexp

let mk_proc frame stm exp =
  (frame, Seq (stm, Move (Temp Assem.eax, exp)))

let rec seq = function
  | [] -> Nop
  | h::[] -> h
  | h::t -> Seq (h, seq t)

let rec stm_to_tree_stm = function
  | Stm_array_assign (s, i, e) ->
    Move (Mem (Binop (Temp (Temp.named ("%" ^ s)),
                      Plus,
                      Binop (Const Assem.word_size,
                             Times,
                             Binop (exp_to_tree_exp i, Plus, Const 1)))),
          exp_to_tree_exp e)
  | Stm_assign (s, e) ->
    Move (Temp (Temp.named ("%" ^ s)), exp_to_tree_exp e)
  | Stm_if (i, t, e) ->
    let l_true = Label.make () in
    let l_false = Label.make () in
    let l_exit = Label.make () in
    seq [translate_cond i l_true l_false;
         Label l_true;
         stm_to_tree_stm t;
         Jump l_exit;
         Label l_false;
         stm_to_tree_stm e;
         Label l_exit; ]
  | Stm_list l -> seq (List.map l stm_to_tree_stm)
  | Stm_print_char e ->
    Exp (Call ((Name (Label.named "L_print_char")), [exp_to_tree_exp e]))
  | Stm_println_int e ->
    Exp (Call ((Name (Label.named "L_println_int")), [exp_to_tree_exp e]))
  | Stm_while (c, b) ->
    let l_again = Label.make () in
    let l_continue = Label.make () in
    let l_done = Label.make () in
    seq [Label l_again;
         translate_cond c l_continue l_done;
         Label l_continue;
         stm_to_tree_stm b;
         Jump l_again;
         Label l_done; ]

and translate_cond c t e = match c with
  | Exp_neg c' -> translate_cond c' e t
  | Exp_true -> Jump t
  | Exp_false -> Jump e
  | Exp_bin_op (a, Syntax.Lt, b) ->
    Cjump (exp_to_tree_exp a, Lt, exp_to_tree_exp b, t, e)
  | Exp_bin_op (a, Syntax.And, b) ->
    let l = Label.make () in
    seq [translate_cond a l e;
         Label l;
         translate_cond b t e]
  | _ ->
    Cjump (exp_to_tree_exp c, Eq, Const 1, t, e);

and exp_to_tree_exp = function
  | Exp_array_get (a, i) ->
    Mem (Binop (exp_to_tree_exp a, Plus,
        (Binop (Const Assem.word_size, Times,
                Binop (Const 1, Plus, (exp_to_tree_exp i))))))
  | Exp_array_length e ->
    Mem (exp_to_tree_exp e)
  | Exp_bin_op (a, Syntax.And, b) ->
    let t_res = Temp.make () in
    let l_true = Label.make () in
    let l_true' = Label.make () in
    let l_false = Label.make () in
    let l_exit = Label.make () in
    Eseq (seq [Cjump (exp_to_tree_exp a, Eq, Const 1, l_true, l_false);
               Label l_true;
               Cjump (exp_to_tree_exp b, Eq, Const 1, l_true', l_false);
               Label l_true';
               Move (Temp t_res, Const 1);
               Jump l_exit;
               Label l_false;
               Move (Temp t_res, Const 0);
               Label l_exit;
              ], Temp t_res)
  | Exp_bin_op (a, Syntax.Lt, b) ->
    let t_res = Temp.make () in
    let l_true = Label.make () in
    let l_false = Label.make () in
    let l_exit = Label.make () in
    Eseq (seq [Cjump (exp_to_tree_exp a, Lt, exp_to_tree_exp b, l_true, l_false);
               Label l_true;
               Move (Temp t_res, Const 1);
               Jump l_exit;
               Label l_false;
               Move (Temp t_res, Const 0);
               Label l_exit;
              ], Temp t_res)
  | Exp_bin_op (a, Syntax.Plus, b) ->
    Binop (exp_to_tree_exp a, Plus, exp_to_tree_exp b)
  | Exp_bin_op (a, Syntax.Minus, b) ->
    Binop (exp_to_tree_exp a, Minus, exp_to_tree_exp b)
  | Exp_bin_op (a, Syntax.Times, b) ->
    Binop (exp_to_tree_exp a, Times, exp_to_tree_exp b)
  | Exp_bin_op (a, Syntax.Div, b) ->
    Binop (exp_to_tree_exp a, Div, exp_to_tree_exp b)
  | Exp_false -> Const 0
  | Exp_id i -> Temp (Temp.named ("%" ^ i))
  | Exp_int_const i -> Const i
  | Exp_invoke (e, classref, meth, a) ->
    (match !classref with
    | Some (Ty_class klass) ->
      Call (Name (Label.named (klass ^ "$" ^ meth)),
            exp_to_tree_exp e :: List.map a ~f:exp_to_tree_exp)
    | _ -> failwith "ICE: no/bad class inferred")
  | Exp_neg e -> Binop (exp_to_tree_exp e, Xor, Const 1)
  | Exp_new s ->
    Call ((Name (Label.named "L_halloc")), [Temp (Temp.named ("sizeof$"^s))])
  | Exp_new_int_array e ->
    let t_len = Temp.make () in
    let t = Temp.make () in
    Eseq (seq [Move (Temp t_len, exp_to_tree_exp e);
               Move (Temp t, Call ((Name (Label.named "L_halloc")),
                                   [Binop (Const Assem.word_size, Times,
                                           Binop (Const 1, Plus, Temp t_len))]));
               Move (Mem (Temp t), Temp t_len)],
          Temp t)
  | Exp_this -> Temp (Temp.named "%this")
  | Exp_true -> Const 1

let rec replace_temporaries_texp repl it =
  match it with
  | Const _ -> it
  | Name _ -> it
  | Temp t ->
    (match LocalTable.find repl (Temp.to_string t) with
    | Some r -> r
    | None -> it)
  | Binop (a,o,b) -> Binop (replace_temporaries_texp repl a,
                            o,
                            replace_temporaries_texp repl b)
  | Mem e -> Mem (replace_temporaries_texp repl e)
  | Call (e, es) -> Call (replace_temporaries_texp repl e,
                          List.map ~f:(replace_temporaries_texp repl) es)
  | Eseq (s, e) -> Eseq (replace_temporaries_tstm repl s,
                         replace_temporaries_texp repl e)

and replace_temporaries_tstm repl it =
  match it with
  | Move (d, s) -> Move (replace_temporaries_texp repl d,
                         replace_temporaries_texp repl s)
  | Exp e -> Exp (replace_temporaries_texp repl e)
  | Jump _ -> it
  | Cjump (e1, o, e2, l1, l2) ->
    Cjump (replace_temporaries_texp repl e1, o,
           replace_temporaries_texp repl e2, l1, l2)
  | Seq (s1, s2) ->
    Seq (replace_temporaries_tstm repl s1,
         replace_temporaries_tstm repl s2)
  | Label _ -> it
  | Nop -> it

let translate_locals frame local_vars =
  List.fold local_vars ~init:(frame, LocalTable.empty) ~f:fun (f, m) l ->
    let (f', t) = alloc_local f in
    (f', LocalTable.add m ~key:("%" ^ l.Syntax.name) ~data:t)

let translate_parameters frame parameters =
  List.foldi ({id = "this"; par_ty = Ty_class "SELF"} :: parameters)
    ~init:LocalTable.empty ~f:fun i m l ->
      let t = param frame i in
      (LocalTable.add m ~key:("%" ^ l.Syntax.id) ~data:t)

let translate_method class_name fieldmap sizemap
    {method_name; parameters; local_vars; body; return_exp} =
  let f = mk_frame (class_name ^ "$" ^ method_name) (List.length parameters) in
  let (f', locals) = translate_locals f local_vars in
  let params = translate_parameters f' parameters in
  let take_left ~key:_ = function
    | `Left x | `Right x | `Both (x, _) -> Some x
  in
  let map = LocalTable.merge ~f:take_left locals params in
  let map' = LocalTable.merge ~f:take_left map fieldmap in
  let map'' = LocalTable.merge ~f:take_left sizemap map' in
  let body_tstm = stm_to_tree_stm body in
  let return_texp = exp_to_tree_exp return_exp in
  mk_proc f' (replace_temporaries_tstm map'' body_tstm)
             (replace_temporaries_texp map'' return_texp)

let mkfieldmap fields =
  List.foldi fields ~init:LocalTable.empty ~f:fun i m f ->
    LocalTable.add m ~key:("%" ^ f.Syntax.name)
      ~data: (Mem (Binop (
        Mem (Binop (Temp Assem.ebp, Plus, Const (2 * Assem.word_size))),
        Plus, Const (i * Assem.word_size))))

let translate_class sizemap {class_name; fields; methods} =
  let fieldmap = mkfieldmap fields in
  List.map methods ~f:(translate_method class_name fieldmap sizemap)

let translate_main {main_body} sizemap =
  let f = mk_frame "Lmain" 1 in
  mk_proc f
    (replace_temporaries_tstm sizemap (stm_to_tree_stm main_body))
    (Const 0)

let mksizemap classes =
  List.fold classes ~init:LocalTable.empty ~f:fun m c ->
    LocalTable.add m ~key:("sizeof$" ^ c.Syntax.class_name)
      ~data:(Const (Assem.word_size * List.length c.Syntax.fields))

let translate_prg {main_class; classes} =
  let sizemap = mksizemap classes in
  translate_main main_class sizemap :: (List.concat_map classes ~f:(translate_class sizemap))
