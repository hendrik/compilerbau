open Core.Std
open Tree

val trace : tree_stm list -> tree_stm list list
val reorder : tree_stm list list -> tree_stm list

val trace_and_reorder : tree_stm list -> tree_stm list
