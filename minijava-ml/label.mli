open Core.Std

module type Unique = sig
  type t with sexp
  val make : unit -> t
  val named : string -> t
  val to_string : t -> string
end

module Label : Unique
module Temp : Unique
