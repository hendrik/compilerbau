%{
open Syntax
%}

/* keywords */
%token BOOLEAN
%token CHAR
%token CLASS
%token ELSE
%token EXTENDS
%token FALSE
%token IF
%token INT
%token LENGTH
%token MAIN
%token NEW
%token PUBLIC
%token RETURN
%token STATIC
%token SYSTEMOUTPRINT
%token SYSTEMOUTPRINTLN
%token THIS
%token TRUE
%token VOID
%token WHILE

/* symbols */
%token ANDAND
%token BANG
%token COMMA
%token DIV
%token DOT
%token EQUALS
%token LBRACE
%token LBRACKET
%token LPAREN
%token LT
%token MINUS
%token PLUS
%token RBRACE
%token RBRACKET
%token RPAREN
%token SEMICOLON
%token TIMES

%token <int> NUM
%token <string> IDENT
%token EOF

%start main_stm
%type <Syntax.prg> main_stm

/* precedences */
%right ANDAND
%nonassoc LT
%left PLUS MINUS
%left TIMES DIV
%left BANG

%%

main_stm: main_class=main_class classes=class_declaration* EOF
      { {main_class; classes;} }

main_class: CLASS main_class_name=ident LBRACE
    PUBLIC STATIC VOID MAIN
    LPAREN main_arg_type=ident LBRACKET RBRACKET main_arg=ident RPAREN
    LBRACE main_body=statement RBRACE RBRACE
      { {main_class_name; main_arg; main_arg_type; main_body; } }

class_declaration: CLASS class_name=ident super_name=extends?
    LBRACE fields=var_declaration* methods=method_declaration* RBRACE
      { {class_name; super_name; fields; methods; } }

extends: EXTENDS ident { $2 }

var_declaration: var_ty=mjtype name=ident SEMICOLON { {var_ty; name;} }

method_declaration: PUBLIC return_ty=mjtype method_name=ident
    LPAREN parameters=separated_list(COMMA, method_arg) RPAREN
    LBRACE vs=vars_then_statements RETURN return_exp=expression SEMICOLON RBRACE
      { {return_ty; method_name; parameters;
         local_vars=fst vs; body=snd vs; return_exp; } }
method_arg: par_ty=mjtype id=ident { {par_ty; id; } }

mjtype: INT LBRACKET RBRACKET { Ty_arr Ty_int }
  | BOOLEAN                   { Ty_bool }
  | INT                       { Ty_int }
  | ident                     { Ty_class $1 }

vars_then_statements:
  | var_declaration vs=vars_then_statements { ($1::(fst vs), snd vs) }
  | statement*                              { ([], Stm_list $1) }

statement: LBRACE statement* RBRACE { Stm_list $2 }
  | IF LPAREN i=expression RPAREN t=statement ELSE e=statement { Stm_if (i,t,e) }
  | WHILE LPAREN c=expression RPAREN b=statement { Stm_while (c,b) }
  | SYSTEMOUTPRINTLN LPAREN e=expression RPAREN SEMICOLON { Stm_println_int e }
  | SYSTEMOUTPRINT LPAREN LPAREN CHAR RPAREN e=expression RPAREN SEMICOLON { Stm_print_char e }
  | i=ident EQUALS e=expression SEMICOLON { Stm_assign (i,e) }
  | i=ident LBRACKET j=expression RBRACKET EQUALS e=expression SEMICOLON { Stm_array_assign (i,j,e) }

expression:
  | BANG expression              { Exp_neg $2 }
  | expression ANDAND expression { Exp_bin_op ($1,And,$3) }
  | expression LT expression     { Exp_bin_op ($1,Lt,$3) }
  | expression PLUS expression   { Exp_bin_op ($1,Plus,$3) }
  | expression MINUS expression  { Exp_bin_op ($1,Minus,$3) }
  | expression TIMES expression  { Exp_bin_op ($1,Times,$3) }
  | expression DIV expression    { Exp_bin_op ($1,Div,$3) }
  | subexpression                { $1 }

subexpression:
  | e=subexpression DOT name=ident LPAREN param=separated_list(COMMA, expression) RPAREN { Exp_invoke (e, ref None, name, param) }
  | e=subexpression DOT LENGTH                     { Exp_array_length e }
  | e=subexpression LBRACKET j=expression RBRACKET { Exp_array_get (e,j) }
  | NUM                                            { Exp_int_const $1 }
  | TRUE                                           { Exp_true }
  | FALSE                                          { Exp_false }
  | ident                                          { Exp_id $1 }
  | THIS                                           { Exp_this }
  | NEW INT LBRACKET e=expression RBRACKET         { Exp_new_int_array e }
  | NEW ident LPAREN RPAREN                        { Exp_new $2 }
  | LPAREN expression RPAREN                       { $2 }

ident: IDENT { $1 }

%%
