open Core.Std

open Typechecker

let parse (file_name: string) =
  In_channel.with_file file_name ~f:(fun ic ->
  let lexbuf = Lexing.from_channel ic in
    try
      Parser.main_stm Lexer.main lexbuf;
    with
    | Parser.Error ->
      let start_pos = lexbuf.Lexing.lex_start_p in
      let msg = Printf.sprintf
          "Parse error at line %i, character %i."
          (start_pos.Lexing.pos_lnum)
          (start_pos.Lexing.pos_cnum - start_pos.Lexing.pos_bol + 1) in
      failwith msg)

let usage_msg = "Usage: test_liveness.byte file1.java [file2.java ...]\n"

let main =
  let no_argument = ref true in
  let print_ast file_name = begin
    let s = parse file_name in
    no_argument := false;
    (match check_prg s with
     | Ok _ -> ();
     | Error msg -> print_endline ("Error: " ^ msg); exit 1);
    print_endline "\t.intel_syntax";
    print_endline "\t.global Lmain";
    print_endline "";
    List.iter (Translate.translate_prg s) ~f:(fun (frame, tstm) ->
      Canonicalize.canon tstm
      |> Trace.trace_and_reorder
      |> Maxmunch.munch
      |> fun f ->
(*
        let m = Liveness.color_prg f in
        print_endline (Sexp.to_string_hum ~indent:2
                         (Liveness.sexp_of_allocgraph m));
*)
      print_endline "";
    );
  end in
  Arg.parse [] print_ast "test_liveness.byte file1.java [file2.java ...]";
  if !no_argument then print_string usage_msg
