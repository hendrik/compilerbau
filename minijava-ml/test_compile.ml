open Core.Std

open Typechecker

let parse (file_name: string) =
  In_channel.with_file file_name ~f:(fun ic ->
  let lexbuf = Lexing.from_channel ic in
    try
      Parser.main_stm Lexer.main lexbuf;
    with
    | Parser.Error ->
      let start_pos = lexbuf.Lexing.lex_start_p in
      let msg = Printf.sprintf
          "Parse error at line %i, character %i."
          (start_pos.Lexing.pos_lnum)
          (start_pos.Lexing.pos_cnum - start_pos.Lexing.pos_bol + 1) in
      failwith msg)

let usage_msg = "Usage: test_compile.byte file1.java [file2.java ...]\n"

let main =
  let no_argument = ref true in
  let print_ast file_name = begin
    let s = parse file_name in
    no_argument := false;
    (match check_prg s with
     | Ok _ -> ();
     | Error msg -> print_endline ("Error: " ^ msg); exit 1);
    print_endline "\t.intel_syntax";
    print_endline "\t.global Lmain";
    print_endline "";
    List.iter (Translate.translate_prg s) ~f:(fun (frame, tstm) ->
      Canonicalize.canon tstm
      |> Trace.trace_and_reorder
      |> Maxmunch.munch
(*
      |> (fun x ->
        print_endline "/****";

      List.iter (Trace.trace_and_reorder (Canonicalize.canon tstm))
        ~f:(fun tstm ->
          print_endline (Tree.tree_stm_to_string tstm));

        print_endline (Sexp.to_string_hum ~indent:2 (sexp_of_graph (mk_graph x)));
        print_endline (Sexp.to_string_hum ~indent:2 (sexp_of_in_out (mk_graph x |> gen_in_out)));
        print_endline "****/";

        x)
*)
      |> Liveness.allocate_registers frame
      |> Frame.frame
      |> Peep.final_peep
      |> List.map ~f:Assem.to_string
      |> String.concat ~sep:"\n"
      |> print_endline;
      print_endline ""
    );
  end in
  Arg.parse [] print_ast "test_compile file1.java [file2.java ...]";
  if !no_argument then print_string usage_msg
