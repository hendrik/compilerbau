#!/bin/bash

for f in \
ArrayAccess \
Precedence \
Scope \
Scope2 \
Sum \
TestEq \
TrivialClass \
Stck \
While \
BubbleSort \
QuickSort \
Effects \
ArrSum \
Primes \
Factorial \
Fib \
FibL \
ShortCutAnd \
LinearSearch \
LinkedList \
BinaryTree \
Graph \
BinarySearch \
Newton \
Mandelbrot \
ManyArgs
do
printf %-10s $f

javac $f.java >/dev/null
/usr/bin/time --format="	comp %U+%S=%e (%P)  " ./test_compile.byte $f.java 2>&1 >$f.s| tr -d '\n'
gcc -g -m32 -o $f runtime.c $f.s

/usr/bin/time --format=" run %U+%S=%e (%P)  " $f 2>&1 >out | tr -d '\n'

java $f | diff out - && echo PASS || echo FAIL
done
