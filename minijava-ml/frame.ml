open Core.Std

open Assem
open Tree


type frame = { name: string;
               param_count: int;
               local_count: int }

let mk_frame name param_count =
  { name; param_count; local_count = 0 }

let param {param_count} i =
  if i <= param_count then
    Mem (Binop (Temp Assem.ebp, Plus, Const ((i+2) * word_size)))
  else
    failwith "ICE: tried to access inexistant parameter"

let stack_size {local_count} =
  word_size * local_count

let alloc_local frame =
  ({frame with local_count = frame.local_count + 1 },
   (Mem (Binop (Temp Assem.ebp, Minus, Const ((frame.local_count+1) * word_size)))))

let alloc_stack frame =
  ({frame with local_count = frame.local_count + 1 },
   (Assem.Mem (mem_make_indirect ebp (-1) ((frame.local_count+1) * word_size))))


let frame (f, prg) =
  List.concat [
    [Assem_label (Label.Label.named f.name);
     Assem_unary_op (PUSH, Reg ebp);
     Assem_binary_op (MOV, Reg ebp, Reg esp);
     Assem_binary_op (SUB, Reg esp, Imm (stack_size f));
     Assem_unary_op (PUSH, Reg ebx);
     Assem_unary_op (PUSH, Reg esi);
     Assem_unary_op (PUSH, Reg edi)];
    prg;
    [Assem_unary_op (POP, Reg edi);
     Assem_unary_op (POP, Reg esi);
     Assem_unary_op (POP, Reg ebx);
     Assem_instr LEAVE;
     Assem_instr RET]]
