open Core.Std
module Map = Core.Core_map

open Assem
open Label
open Frame

module IntMap = Map.Make(Int)
module IntSet = Set.Make(Int)
module TempMap = Map.Make(struct
  type t = Temp.t with sexp
  let compare = compare end)
module TempSet = Set.Make(struct
  type t = Temp.t with sexp
  let compare = compare end)

module TempTempSet = Set.Make(struct
  type t = (Temp.t * Temp.t) with sexp
  let compare = function                (* Unordered graph.  *)
    | (x, y)  when x < y -> compare (x, y)
    | (x, y)             -> compare (y, x)
end)

type graph = (assem * int list) list with sexp

type intmap = TempSet.t IntMap.t with sexp
type intset = IntSet.t with sexp
type in_out = (intmap * intmap) with sexp
type infgraph = TempTempSet.t with sexp
type node_count = int TempMap.t with sexp
type allocgraph = Temp.t TempMap.t with sexp
type filter_node_count = (Temp.t * int) list * (Temp.t * int) list with sexp

let rec mem y = function
  | [] -> false
  | x :: _ when x = y -> true
  | _ :: xs -> mem y xs

let listrem s r =
  List.filter s (fun x -> not (mem x r))

let rec attach l i e = match l with
  | (x, r) :: xs when i = 0 -> (x, e :: r) :: xs
  | x :: xs -> x :: attach xs (i-1) e
  | [] -> []

let mk_graph prg =
  let graph = List.map prg (fun insn -> (insn, [])) in
  List.foldi prg ~init:graph ~f:(fun i a e ->
    let a' = if is_fall_through e then attach a i (i+1) else a in
    match jumps e with
    | [l] ->
      (match List.findi prg (fun _ e' ->
        match is_label e' with
        | Some l' when l = l' -> true
        | _ -> false) with
      | Some (i', _) -> attach a' i i'
      | None -> a')
    | _ -> a')

let gen_in_out ary =
  let step inn out =
    List.foldi ary ~init:(inn, out) ~f:(fun here (inn', out') (inst, succ) ->
      let out'' = TempSet.union_list
        (List.map succ (fun i ->
          IntMap.find_exn inn' i)) in
      let in'' = TempSet.union
        (TempSet.of_list (use inst))
        (TempSet.diff out'' (TempSet.of_list (def inst))) in
      (IntMap.add inn' here in'',
       IntMap.add out' here out'')
    )
  in
  let rec recurse inn out =
    let (inn', out') = step inn out in
    if IntMap.equal TempSet.equal inn inn' &&
       IntMap.equal TempSet.equal out out' then
      (inn', out')
    else
      recurse inn' out'
  in
  let max = List.length ary + 1 in
  recurse
    (IntMap.of_alist_exn (List.init max (fun i -> (i, TempSet.empty))))
    (IntMap.of_alist_exn (List.init max (fun i -> (i, TempSet.empty))))

let add_node g t u =
  let g' = TempTempSet.add g (t, u) in
  let g'' = TempTempSet.add g' (t, t) in
  TempTempSet.add g'' (u, u)

let inference prg out =
  let graph = TempTempSet.empty in
  List.foldi prg ~init:graph ~f:(fun i graph' e ->
    match is_move_between_temps e with
    | None ->
      List.fold (def e) ~init:graph' ~f:(fun graph'' t ->
        TempSet.fold (IntMap.find_exn out i) ~init:graph'' ~f:(fun graph''' u ->
          add_node graph''' t u))
    | Some (t, v) ->
      TempSet.fold (IntMap.find_exn out i) ~init:graph' ~f:(fun graph'' u ->
        if v = u then
          graph''
        else
          add_node graph'' t u))

let leftover l =
  TempSet.min_elt (TempSet.diff
                     (TempSet.of_list free_reg)
                     (TempSet.of_list (TempMap.data l)))

let restrict_coloring c r =
  TempMap.filter c ~f:(fun ~key:k ~data:_ ->
    TempSet.mem r k)

let spill_map m =
  TempMap.fold m ~init:TempMap.empty ~f:fun ~key:r ~data:r' m ->
    TempMap.add m r (Reg r')

let spill_prg prg m =
  let m' = spill_map m in
  let sigma = fun x ->
    match TempMap.find m' x with
    | Some (Reg y) -> y
    | Some _ -> failwith "ICE: spilled not-a-register"
    | None -> unneeded_reg
  in
  List.map prg (rename sigma)

let node_count g removed =
  TempTempSet.fold g ~init:TempMap.empty ~f:(fun m (e1,e2) ->
    if mem e1 removed || mem e2 removed
    then m
    else
      TempMap.change m e1 (function
      | None when e1 = e2 -> Some 0
      | None -> Some 1
      | Some i when e1 = e2 -> Some i
      | Some i -> Some (i+1))
    |> fun m' ->
      TempMap.change m' e2 (function
      | None when e1 = e2 -> Some 0
      | None -> Some 1
      | Some i when e1 = e2 -> Some i
      | Some i -> Some (i+1)))

let filter_node_count g removed maxv =
  List.partition_tf 
    (List.sort ~cmp:(fun (_,i) (_,j) -> compare i j)
       (TempMap.to_alist (node_count g removed))) ~f:fun (_, v) -> v < maxv

let neighbors g removed v =
  TempTempSet.fold g ~init:TempSet.empty ~f:(fun n (e1,e2) ->
    if (e1 = v || e2 = v) && e1 <> e2 &&
      not (mem e1 removed) && not (mem e2 removed)
    then TempSet.add (TempSet.add n e1) e2
    else n)

let build =
  List.fold all_reg ~init:TempMap.empty ~f:(fun m r -> TempMap.add m r r)

exception Spill of Temp.t

let rec select g a = function
  | [] -> a
  | x :: xs ->
    if mem x all_reg then
      select g a xs
    else (
      print_endline ("## allocating " ^ (Temp.to_string x) ^ " with " ^ (string_of_int (TempSet.length (neighbors g xs x))) ^ " neigh");

      match leftover (restrict_coloring a (neighbors g xs x)) with
      | Some i ->
        print_endline ("## allocating " ^ (Temp.to_string x) ^ " as " ^ (Temp.to_string i));
        select g (TempMap.add a x i) xs
      | None -> (* need to spill.  *)
        print_endline ("## spilling " ^ (Temp.to_string x));
      (* select g (TempMap.add a x spill_me) xs *)
        raise (Spill x)
    )

let rec simplify g a s =
  match filter_node_count g s (List.length free_reg) with
  | [], [] -> select g a (List.append all_reg (List.rev (listrem s all_reg)))
  | [], ((w,_) :: _) -> simplify g a (w :: s)
  | (v,_)::_, _ -> simplify g a (v :: s)

let spill_assem t loc insn =
  if mem t (use insn) || mem t (def insn) then
    let t' = Temp.make () in
    [Assem_binary_op (MOV, Reg t', loc);
     rename (fun x -> if x = t then t' else x) insn;
     Assem_binary_op (MOV, loc, Reg t')]
  else
    [insn]

let spill frame t prg =
  let (frame', t') = alloc_stack frame in
  (frame', List.concat_map prg (spill_assem t t'))

let rec allocate_registers frame prg =
  try
    let (_, out) = gen_in_out (mk_graph prg) in
    let g = inference prg out in
    let a = simplify g build [] in
    (frame, spill_prg prg a)
  with Spill t ->
    let (frame', prg') = spill frame t prg in
    allocate_registers frame' prg'

(*
let _ =
  let t_a = Temp.named "a" in
  let t_b = Temp.named "b" in
  let t_c = Temp.named "c" in
  let t_d = Temp.named "d" in
  let prg = [
    Assem_jump (CALL, None, None, Some (Label.named "xxx"));
    Assem_binary_op (MOV, Reg t_a, Reg eax);
    Assem_binary_op (MOV, Reg t_b, Reg t_a);
  ] in
  let (inn, out) = mk_graph prg |> gen_in_out in
  print_endline (Sexp.to_string_hum ~indent:2 (sexp_of_graph (mk_graph prg)));
  print_endline (Sexp.to_string_hum ~indent:2 (sexp_of_in_out (mk_graph prg |> gen_in_out)));
  print_endline (Sexp.to_string_hum ~indent:2 (sexp_of_allocgraph (color (inference prg out) TempMap.empty)))
*)

(*
let _ =
  let prg = [Assem_instr NOP;
             Assem_instr NOP;
             Assem_binary_op (MOV, Reg eax, Reg edx);
             Assem_label (Label.named "foo");
             Assem_jump (JMP, None, None, Some (Label.named "foo"));
             Assem_jump (J, Some E, None, Some (Label.named "foo"));
             Assem_instr RET] in
  print_endline (Sexp.to_string_hum ~indent:2 (sexp_of_graph (mk_graph prg)));
  print_endline (Sexp.to_string_hum ~indent:2 (sexp_of_in_out (mk_graph prg |> gen_in_out)));

  print_endline "";

  let t_a = Temp.named "a" in
  let t_b = Temp.named "b" in
  let t_c = Temp.named "c" in
  let t_d = Temp.named "d" in

  let prg = [Assem_binary_op (MOV, Reg t_a, Imm 0);
             Assem_label (Label.named "L1");

             Assem_binary_op (MOV, Reg t_b, Reg t_a);
             Assem_unary_op (INC, Reg t_b);

             Assem_binary_op (ADD, Reg t_c, Reg t_b);

             Assem_binary_op (MOV, Reg t_a, Reg t_b);
             Assem_binary_op (IMUL, Reg t_a, Imm 2);

             Assem_binary_op (CMP, Reg t_a, Imm 42);
             Assem_jump (J, Some L, None, Some (Label.named("L1")));

             Assem_binary_op (MOV, Reg eax, Reg t_c);
             Assem_instr RET] in

  let (inn, out) = mk_graph prg |> gen_in_out in
  print_endline (Sexp.to_string_hum ~indent:2 (sexp_of_in_out (inn, out)));
  print_endline (Sexp.to_string_hum ~indent:2 (sexp_of_infgraph (inference prg out)));

  print_endline (Sexp.to_string_hum ~indent:2 (sexp_of_node_count (node_count (inference prg out))));
  print_endline "w/o a:";
  print_endline (Sexp.to_string_hum ~indent:2
                   (sexp_of_infgraph
                      (remove_node
                         (inference prg out)
                         t_a)));
  print_endline "w/o b:";
  print_endline (Sexp.to_string_hum ~indent:2
                   (sexp_of_infgraph
                      (remove_node
                         (inference prg out)
                         t_b)));
  print_endline "w/o c:";
  print_endline (Sexp.to_string_hum ~indent:2
                   (sexp_of_infgraph
                      (remove_node
                         (inference prg out)
                         t_c)));
(*  print_endline (Sexp.to_string_hum ~indent:2 (sexp_of_filter_node_count (filter_node_count (inference prg out) ))); *)
  print_endline (Sexp.to_string_hum ~indent:2 (sexp_of_allocgraph (color (inference prg out)  TempMap.empty)))
*)
