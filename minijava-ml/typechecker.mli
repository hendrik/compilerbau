open Core.Std
open Syntax

type classtable

val compute_classtable : prg -> (classtable, string) Result.t
val classtable_to_string : classtable -> string

val check_prg : prg -> (unit, string) Result.t
