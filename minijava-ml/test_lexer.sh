#!/bin/sh
set -e

check() {
  msg=$1; shift
  if eval "$@" >/dev/null; then
    echo ok - $msg
  else 
    echo fail - $msg
  fi
}

for f in ../MiniJava-Beispiele/{Small,Large}/*.java; do
  check "lex $f" ./test_lexer.byte $f
done
for f in ../MiniJava-Beispiele/ShouldFail/ParseErrors/{BrokenLex*,StringLiteral}.java; do
  check "detect syntax error in $f" "! ./test_lexer.byte $f 2>/dev/null"
done
