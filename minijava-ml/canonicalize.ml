open Core.Std

open Tree
open Label

let (++) = List.append

let split_effect it = match it with
  | Const _
  | Name _
  | Temp _ -> ([], it)
  | _ ->
    let t_it = Temp.make () in
    ([Move (Temp t_it, it)], Temp t_it)


let rec fold_stm it = match it with
  | Move (dst, src) -> Move (fold_exp dst, fold_exp src)
  | Exp e -> Exp (fold_exp e)
  | Jump _ -> it
  | Cjump _ -> it
  | Label _ -> it
  | Nop -> it
  | Seq (a, b) -> Seq (fold_stm a, fold_stm b)

and fold_exp it = match it with
  | Const _ -> it
  | Name _ -> it
  | Temp _ -> it
  | Binop (Const a, Plus, Const b) -> Const (a+b)
  | Binop (Const a, Minus, Const b) -> Const (a-b)
  | Binop (Const a, Times, Const b) -> Const (a*b)
  | Binop (Const a, Div, Const b) -> Const (a/b)
  | Binop (a, o, b) -> fold_exp_again (Binop (fold_exp a, o, fold_exp b))
  | Tree.Mem m -> Tree.Mem (fold_exp m)
  | Call (Name f, xs) -> Call (Name f, List.map xs fold_exp)
  | Call _ -> failwith "ICE: invalid call generated"
  | Eseq (s, e) -> Eseq (fold_stm s, fold_exp e)

and fold_exp_again it = match it with
  | Binop (Const _, _, Const _) -> fold_exp it
  | _ -> it

let rec canon_texp = function
  | Const i -> ([], Const i)
  | Name n -> ([], Name n)
  | Temp t -> ([], Temp t)
  | Binop (a, o, b) ->
    let (s_a', a') = canon_texp a in
    let (s_b', b') = canon_texp b in
    let (s_a'', a'') = split_effect a' in
    let (s_b'', b'') = split_effect b' in
    (s_a' ++ s_a'' ++ s_b' ++ s_b'',
     Binop (a'', o, b''))
  | Mem e ->
    let (s_e', e') = canon_texp e in
    (s_e', Mem e')
  | Call (e, es) ->
    let t_r = Temp.make () in
    let (s_e', e') = canon_texp e in
    let (ss, es') = List.fold es ~init:(s_e', []) ~f:(fun (ss, es') e ->
      let (s_e1, e1) = canon_texp e in
      let (s_e1', e1') = split_effect e1 in
      (ss ++ s_e1 ++ s_e1', es' ++ [e1']))
    in
    (ss ++ [Move (Temp t_r, Call (e', es'))], Temp t_r)
  | Eseq (s, e) ->
    let s' = canon_tstm s in
    let (s_e', e') = canon_texp e in
    (s' ++ s_e', e')

and canon_tstm = function
  | Move (a, b) ->
    let (s_a', a') = canon_texp a in
    let (s_b', b') = canon_texp b in
    s_a' ++ s_b' ++ [Move (a', b')]
  | Exp e ->
    let (s_e', e') = canon_texp e in
    s_e' ++ [Exp e']
  | Jump l -> [Jump l]
  | Cjump (a, o, b, i, t) ->
    let (s_a', a') = canon_texp a in
    let (s_b', b') = canon_texp b in
    s_a' ++ s_b' ++ [Cjump (a', o, b', i, t)]
  | Seq (a, b) ->
    canon_tstm a ++ canon_tstm b
  | Label l -> [Label l]
  | Nop -> [Nop]

let canon stm = canon_tstm (fold_stm stm)
