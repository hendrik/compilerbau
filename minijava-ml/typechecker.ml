open Core.Std
module Map = Core.Core_map
open Result.Monad_infix

open Syntax

module ClassTable = Map.Make (String)
module MethTable  = Map.Make (String)
module LocalTable = Map.Make (String)
module FieldTable = Map.Make (String)

type localtable = ty LocalTable.t with sexp

type methinfo = { meth_method: declmeth; 
                  meth_locals: localtable } with sexp

type methtable = methinfo MethTable.t with sexp
type fieldtable = declvar FieldTable.t with sexp

type info = { info_class: declclass;
              info_fields: fieldtable;
              info_methods: methtable } with sexp

type classtable = info ClassTable.t with sexp

let classtable_to_string p = Sexp.to_string_hum (sexp_of_classtable p)

let compute_localtable tbl lvars params =
  with_return (fun r ->
    let ptbl =
      List.fold params ~init:LocalTable.empty
        ~f:(fun tbl {id; par_ty} ->
          LocalTable.change tbl id (function
            | None -> Some par_ty
            | Some _ -> r.return (Error ("already defined parameter " ^ id))))
    and ltbl =
      List.fold lvars ~init:LocalTable.empty
        ~f:(fun tbl {name; var_ty} ->
          LocalTable.change tbl name (function
            | None -> Some var_ty
            | Some _ -> r.return (Error ("already defined variable " ^ name))))
    in
    let take_left ~key:_ = function
      | `Left x | `Right x | `Both (x, _) -> Some x
    in
    Ok (LocalTable.merge ~f:take_left
        (LocalTable.merge ~f:take_left tbl ptbl) ltbl))

let compute_methtable cls =
  with_return (fun r ->
    let ltbl = LocalTable.add LocalTable.empty "this" (Ty_class cls.class_name) in
    Ok (List.fold cls.methods ~init:MethTable.empty
        ~f:fun tbl meth ->
          MethTable.change tbl meth.method_name (function
            | None ->
              (match compute_localtable ltbl meth.local_vars meth.parameters with
               | Ok locals -> Some { meth_method = meth; meth_locals = locals }
               | Error _ as e -> r.return e)
            | Some _ -> r.return (Error
                            ("already defined method " ^ meth.method_name)))))

let compute_fieldtable {fields} =
  with_return (fun r ->
    Ok (List.fold fields ~init:MethTable.empty
        ~f:fun tbl field ->
          MethTable.change tbl field.name (function
            | None -> Some field
            | Some _ -> r.return (Error
                            ("already defined field " ^ field.name)))))

let compute_classtable {classes} =
  with_return (fun r ->
    Ok (List.fold classes ~init:ClassTable.empty
        ~f:fun tbl cls ->
          match
            compute_methtable cls >>= fun methods ->
            compute_fieldtable cls >>= fun fields ->
            Ok (ClassTable.change tbl cls.class_name (function
                | None -> Some { info_class = cls;
                                 info_methods = methods;
                                 info_fields = fields }
                | Some _ -> r.return (Error
                                ("already defined class " ^ cls.class_name))))
          with
          | Ok t -> t
          | Error _ as e -> r.return e))

let check_main_class {main_arg_type} =
  if main_arg_type = "String"
  then Ok ()
  else Error "argument type of main function is not String"

let rec check_valid_type tbl = function
  | Ty_bool -> Ok ()
  | Ty_int -> Ok ()
  | Ty_arr t -> check_valid_type tbl t
  | Ty_class c ->
    if ClassTable.mem tbl c
    then Ok ()
    else Error ("undefined class name " ^ c)

let combine_errors l =
  List.fold l ~init:(Ok ()) ~f:fun a e ->
    match a, e with
    | Ok (), e -> e
    | Error s, Ok () -> Error s
    | Error s, Error t -> Error (s ^ "\n" ^ t)

let check_class_name tbl cls =
  match ClassTable.find tbl cls with 
  | Some _ -> Ok (Ty_class cls)
  | None -> Error ("no such class: " ^ cls)          

let check_field tbl {var_ty} =
  check_valid_type tbl var_ty

let check_parameter tbl {par_ty} =
  check_valid_type tbl par_ty

let check_fields tbl vars =
  List.map vars (check_field tbl)

let check_valid_local_vars tbl vars =
  List.map vars (check_field tbl)

let check_valid_parameters tbl vars =
  List.map vars (check_parameter tbl)

let check_lvar _tbl ltbl name =
  match LocalTable.find ltbl name with
  | Some t -> Ok t
  | None -> Error ("undefined local variable: " ^ name)

let check_expected_type ?(msg="type mismatch") a b =
  if a = b
  then Ok ()
  else Error (msg ^ ": wanted " ^ ty_to_string a ^
              " got " ^ ty_to_string b)

let check_expected_return_type =
  check_expected_type ~msg:"return type mismatch"

let check_array = function
  | Ty_arr t_v -> Ok t_v
  | _ -> Error "trying to dereference a non-array type"

let check_unqualified_field tbl ltbl i =
  check_lvar tbl ltbl "this" >>= function
  | Ty_class t_this ->
    (let {info_fields} = ClassTable.find_exn tbl t_this in
     match FieldTable.find info_fields i with
     | Some {var_ty} -> Ok var_ty
     | None -> Error ("unknown field: " ^ i))
  | _ -> failwith "ICE: this not of type class"

let check_id tbl ltbl i =
  match check_lvar tbl ltbl i with
  | Ok t -> Ok t
  | Error _ -> check_unqualified_field tbl ltbl i

let rec check_exp tbl ltbl = function
  | Exp_array_get (a, i) ->
    check_exp tbl ltbl a >>= fun t_a ->
    check_array t_a >>= fun t_v ->
    check_exp tbl ltbl i >>= fun t_i ->
    check_expected_type Ty_int t_i >>= fun _ ->
    Ok t_v
  | Exp_array_length a ->
    check_exp tbl ltbl a >>= fun t_a ->
    check_array t_a >>= fun _ ->
    Ok Ty_int
  | Exp_bin_op (a, op, b) ->
    check_exp tbl ltbl a >>= fun t_a ->
    check_exp tbl ltbl b >>= fun t_b ->
    let (in_ty, out_ty) = match op with
      | Plus | Minus | Times | Div -> (Ty_int, Ty_int)
      | And -> (Ty_bool, Ty_bool)
      | Lt -> (Ty_int, Ty_bool)
    in
    check_expected_type in_ty t_a >>= fun _ ->
    check_expected_type in_ty t_b >>= fun _ ->
    Ok out_ty
  | Exp_false -> Ok Ty_bool
  | Exp_id i -> check_id tbl ltbl i
  | Exp_int_const _ -> Ok Ty_int
  | Exp_invoke (e, classref, name, param) ->
    check_exp tbl ltbl e >>= fun t_e ->
    check_invoke tbl ltbl t_e name param >>= fun t_r ->
    classref := Some t_e;
    Ok t_r
  | Exp_neg e ->
    check_exp tbl ltbl e >>= fun t_e ->
    check_expected_type Ty_bool t_e >>= fun _ ->
    Ok Ty_bool
  | Exp_new c ->
    check_class_name tbl c
  | Exp_new_int_array l ->
    check_exp tbl ltbl l >>= fun t_l ->
    check_expected_type Ty_int t_l >>= fun _ ->
    Ok (Ty_arr Ty_int)
  | Exp_this ->
    check_lvar tbl ltbl "this"
  | Exp_true ->
    Ok Ty_bool

and check_invoke tbl ltbl t_e name param =
  match t_e with
  | Ty_class c ->
      (let {info_methods} = ClassTable.find_exn tbl c in
       match MethTable.find info_methods name with
       | None -> Error ("undefined method " ^ name ^ " for " ^ c)
       | Some {meth_method} ->
         match List.zip param meth_method.parameters with
         | None -> Error ("invalid number of arguments for " ^ name)
         | Some zipped -> combine_errors (List.map zipped (fun (e, {par_ty}) ->
                              check_exp tbl ltbl e >>= fun t_e ->
                              check_expected_type par_ty t_e)) >>= fun _ ->
                          Ok meth_method.return_ty)
  | _ -> Error "method invoke on non-object"

let rec check_stm tbl ltbl = function
  | Stm_array_assign (name, i, e) ->
    check_id tbl ltbl name >>= fun t_name ->
    check_array t_name >>= fun t_name_v ->
    check_exp tbl ltbl i >>= fun t_i ->
    check_expected_type Ty_int t_i >>= fun _ ->
    check_exp tbl ltbl e >>= fun t_e ->
    check_expected_type t_name_v t_e
      
  | Stm_assign (name, e) ->
    check_id tbl ltbl name >>= fun t_name ->
    check_exp tbl ltbl e >>= fun t_e ->
    check_expected_type t_name t_e

  | Stm_if (b, t, e) ->
    check_exp tbl ltbl b >>= fun t_b ->
    check_expected_type Ty_bool t_b >>= fun _ ->
    check_stm tbl ltbl t >>= fun _ ->
    check_stm tbl ltbl e

  | Stm_list (h::t) ->
    check_stm tbl ltbl h >>= fun _ ->
    check_stm tbl ltbl (Stm_list t)

  | Stm_list [] -> Ok ()

  | Stm_print_char e ->
    check_exp tbl ltbl e >>= fun t_e ->
    check_expected_type Ty_int t_e

  | Stm_println_int e ->
    check_exp tbl ltbl e >>= fun t_e ->
    check_expected_type Ty_int t_e

  | Stm_while (b, e) ->
    check_exp tbl ltbl b >>= fun t_b ->
    check_expected_type Ty_bool t_b >>= fun _ ->
    check_stm tbl ltbl e

let check_method tbl {meth_method; meth_locals = ltbl} =
  let {return_ty; return_exp; parameters; local_vars; body} = meth_method in
  check_valid_type tbl return_ty >>= fun _ ->
  combine_errors (check_valid_parameters tbl parameters) >>= fun _ ->
  combine_errors (check_valid_local_vars tbl local_vars) >>= fun _ ->
  check_stm tbl ltbl body >>= fun _ ->
  check_exp tbl ltbl return_exp >>= fun t_re ->
  check_expected_return_type return_ty t_re

let check_class tbl {fields} info_methods =
  combine_errors (check_fields tbl fields) >>= fun _ ->
  combine_errors (List.map (MethTable.data info_methods) (check_method tbl))

let check_prg prg =
  compute_classtable prg >>= fun tbl ->
  check_main_class prg.main_class >>= fun _ ->
  combine_errors
    (List.map (ClassTable.data tbl)
      (fun {info_class; info_methods} ->
        check_class tbl info_class info_methods)) >>= fun _ ->
  check_stm tbl LocalTable.empty prg.main_class.main_body  (* no this *)
