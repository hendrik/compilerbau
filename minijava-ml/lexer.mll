{
open Parser
open Lexing

let incr_linenum lexbuf =
  let pos = lexbuf.Lexing.lex_curr_p in
  lexbuf.Lexing.lex_curr_p <- { pos with
    Lexing.pos_lnum = pos.Lexing.pos_lnum + 1;
    Lexing.pos_bol = pos.Lexing.pos_cnum;
  }

let error lexbuf msg =
  let pos = lexbuf.Lexing.lex_curr_p in
  print_string msg;
  Printf.printf " at position %i.\n" (pos.pos_cnum - pos.pos_bol);
  flush stdout;
  failwith "Lexer error"
}

let white = [' ' '\t']+
let num = ['0'-'9']
let alpha = ['a'-'z'] | ['A'-'Z'] | '_'
let int = num+
let ident = alpha (alpha | num)*

rule main = parse
  | '\n'       { incr_linenum lexbuf; main lexbuf }
  |  white     { main lexbuf }

  | "&&"       { ANDAND }
  | '!'        { BANG }
  | '('        { LPAREN }
  | ')'        { RPAREN }
  | '*'        { TIMES }
  | '+'        { PLUS }
  | ','        { COMMA }
  | '-'        { MINUS }
  | '.'        { DOT }
  | '/'        { DIV }
  | ';'        { SEMICOLON }
  | '<'        { LT }
  | '='        { EQUALS }
  | '['        { LBRACKET }
  | ']'        { RBRACKET }
  | '{'        { LBRACE }
  | '}'        { RBRACE }

  | "System.out.print"   { SYSTEMOUTPRINT }
  | "System.out.println" { SYSTEMOUTPRINTLN }
  | "boolean"            { BOOLEAN }
  | "char"               { CHAR }
  | "class"              { CLASS }
  | "else"               { ELSE }
  | "extends"            { EXTENDS }
  | "false"              { FALSE }
  | "length"             { LENGTH }
  | "if"                 { IF }
  | "int"                { INT }
  | "main"               { MAIN }
  | "new"                { NEW }
  | "public"             { PUBLIC }
  | "return"             { RETURN }
  | "static"             { STATIC }
  | "this"               { THIS }
  | "true"               { TRUE }
  | "void"               { VOID }
  | "while"              { WHILE }

  | ident      { IDENT (Lexing.lexeme lexbuf) }
  | int        { NUM (int_of_string (Lexing.lexeme lexbuf)) }
  | eof        { EOF }
  | "/*"       { comments 0 lexbuf }
  | "//"       { line_comment lexbuf }
  | _          { error lexbuf "Unexpected symbol" }
and comments level = parse
  | '\n'       { incr_linenum lexbuf; comments level lexbuf }
  | "/*"       { comments (level+1) lexbuf }
  | "*/"       { if level = 0 then main lexbuf else comments (level-1) lexbuf }
  | _          { comments level lexbuf }
  | eof        { print_endline "comments are not closed"; raise End_of_file }
and line_comment = parse
  | '\n'       { incr_linenum lexbuf; main lexbuf }
  | _          { line_comment lexbuf }
