open Core.Std

open Label
open Tree
open Assem

let (++) = List.append

let op_to_asm = function
  | Plus -> ADD
  | Minus -> SUB
  | Times -> IMUL
  | And -> AND
  | Or -> OR
  | Xor -> XOR
  | Div -> failwith "ICE: op_to_asm called for division"

let sgn = function
  | Plus -> 1
  | Minus -> -1
  | _ -> failwith "can't happen"

let rec munch_stm = function
  | Move (Tree.Mem (Binop (m, (Plus|Minus as o), Const i)), Const src) ->
    let (ops, t_mem) = munch_exp m in
    ops ++
      [Assem_binary_op (MOV, Assem.Mem (mem_make_indirect t_mem (sgn o) i),
                        Imm src)]
  | Move (Tree.Mem (Binop (m, (Plus|Minus as o), Const i)), src) ->
    let (op_src, t_src) = munch_exp src in
    let (ops, t_mem) = munch_exp m in
    op_src ++ ops ++
      [Assem_binary_op (MOV, Assem.Mem (mem_make_indirect t_mem (sgn o) i),
                        Reg t_src)]
  | Move (Tree.Mem m, src) ->
    let (op_dst, t_dst) = munch_exp m in
    let (op_src, t_src) = munch_exp src in
    op_dst ++ op_src ++
      [Assem_binary_op (MOV, Assem.Mem (mem_make t_dst), Reg t_src)]
  | Move (dst, src) ->
    let (op_src, t_src) = munch_exp src in
    let (op_dst, t_dst) = munch_exp dst in
    op_src ++ op_dst ++ [Assem_binary_op (MOV, Reg t_dst, Reg t_src)]
  | Exp e ->
    let (op, _) = munch_exp e in
    op
  | Jump l ->
    [Assem_jump (JMP, None, None, Some l)]
  | Cjump (a, o, b, t, e) ->
    let (op_a, t_a) = munch_exp a in
    let (op_b, t_b) = munch_exp b in
    op_a ++ op_b ++ 
      [Assem_binary_op (CMP, Reg t_a, Reg t_b);
       Assem_jump (J, Some (match o with
         Eq -> E | Ne -> NE | Lt -> L | Gt -> G | Le -> LE | Ge -> GE),
                   None, Some t);
       Assem_jump (JMP, None, None, Some e)]
  | Label l ->
    [Assem_label l]
  | Nop ->
    [Assem_instr NOP]
  | Seq _ -> failwith "ICE: Maxmunch.munch_stm used on uncanonicalized stm"

and munch_exp = function
  | Const i ->
    let t_imm = Temp.make () in
    ([Assem_binary_op (MOV, Reg t_imm, Imm i)], t_imm)
  | Name _ -> failwith "ICE: tried to munch_exp Name"
  | Temp t -> ([], t)
  | Binop (a, Plus, Const 1)
  | Binop (a, Minus, Const -1) ->
    let (op_a, t_a) = munch_exp a in
    let t_res = Temp.make () in
    (op_a ++
       [Assem_binary_op (MOV, Reg t_res, Reg t_a);
        Assem_unary_op (INC, Reg t_res)],
     t_res)
  | Binop (a, Plus, Const -1)
  | Binop (a, Minus, Const 1) ->
    let (op_a, t_a) = munch_exp a in
    let t_res = Temp.make () in
    (op_a ++
       [Assem_binary_op (MOV, Reg t_res, Reg t_a);
        Assem_unary_op (DEC, Reg t_res)],
     t_res)
  | Binop (a, Div, b) ->
    let (op_a, t_a) = munch_exp a in
    let (op_b, t_b) = munch_exp b in
    let t_res = Temp.make () in
    (op_a ++ op_b ++
       [Assem_binary_op (MOV, Reg eax, Reg t_a);
        Assem_instr CDQ;
        Assem_unary_op (IDIV, Reg t_b);
        Assem_binary_op (MOV, Reg t_res, Reg eax)],
     t_res)
  | Binop (a, o, Const i) ->
    let (op_a, t_a) = munch_exp a in
    let t_res = Temp.make () in
    (op_a ++
       [Assem_binary_op (MOV, Reg t_res, Reg t_a);
        Assem_binary_op (op_to_asm o, Reg t_res, Imm i)],
     t_res)
  | Binop (Const i, Plus, b) -> munch_exp (Binop (b, Plus, Const i))
  | Binop (Const i, Times, b) -> munch_exp (Binop (b, Times, Const i))
  | Binop (a, o, b) ->
    let (op_a, t_a) = munch_exp a in
    let (op_b, t_b) = munch_exp b in
    let t_res = Temp.make () in
    (op_a ++ op_b ++
       [Assem_binary_op (MOV, Reg t_res, Reg t_a);
        Assem_binary_op (op_to_asm o, Reg t_res, Reg t_b)],
     t_res)
  | Tree.Mem (Binop (m, (Plus|Minus as o), Const i)) ->
    let t_res = Temp.make () in
    let (ops, t_mem) = munch_exp m in
    (ops ++ [Assem_binary_op (MOV, Reg t_res,
                              Assem.Mem (mem_make_indirect t_mem (sgn o) i))], t_res)
  | Tree.Mem m ->
    let t_res = Temp.make () in
    let (ops, t_mem) = munch_exp m in
    (ops ++ [Assem_binary_op (MOV, Reg t_res, Assem.Mem (mem_make t_mem))], t_res)
  | Call (Name f, xs) ->
    let t_res = Temp.make () in
    let (ops, tmps) = List.fold xs ~init:([], []) ~f:(fun (ops,tmps) e ->
      let (ops', tmp) = munch_exp e in
      (ops ++ ops', tmp :: tmps)) in
    (ops ++
       List.map tmps (fun x -> Assem_unary_op (PUSH, Reg x)) ++
       [Assem_jump (CALL, None, None, Some f);
        Assem_binary_op (ADD, Reg esp, Imm (Assem.word_size * List.length xs));
        Assem_binary_op (MOV, Reg t_res, Reg eax)],
     t_res)
  | Call _ -> failwith "ICE: invalid Call in Maxmunch.munch_stm"
  | Eseq _ -> failwith "ICE: Maxmunch.munch_stm used on uncanonicalized exp"

let munch l = List.concat (List.map l munch_stm)
