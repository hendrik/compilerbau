open Core.Std

open Label

type mem = { base: Temp.t option;
             scale: int option;
             index: Temp.t option;
             displacement: int; } with sexp

type operand =
  | Imm of int
  | Reg of Temp.t
  | Mem of mem
with sexp

type assem =  
  | Assem_jump of jump_kind * cond option * operand option * Label.t option
  | Assem_unary_op of unary_kind * operand
  | Assem_label of Label.t
  | Assem_binary_op of binary_kind * operand * operand
  | Assem_instr of instr_kind
and jump_kind = JMP | J | CALL
and unary_kind = PUSH | POP | NEG | NOT | INC | DEC | IDIV | ENTER
and binary_kind = MOV | ADD | SUB | IMUL | SHL | SHR | SAL | SAR | AND | OR | XOR | TEST | CMP | LEA
and instr_kind = RET | LEAVE | NOP | CDQ
and cond = E | NE | L | LE | G | GE | Z
with sexp

val word_size : int

val eax : Temp.t
val ebx : Temp.t
val ecx : Temp.t
val edx : Temp.t
val esi : Temp.t
val edi : Temp.t
val esp : Temp.t
val ebp : Temp.t

val free_reg : Temp.t list
val all_reg : Temp.t list
val unneeded_reg : Temp.t

val mem_make : Temp.t -> mem
val mem_make_indirect : Temp.t -> int -> int -> mem

val use : assem -> Temp.t list
val def : assem -> Temp.t list
val jumps : assem -> Label.t list

val is_fall_through : assem -> bool
val is_move_between_temps : assem -> (Temp.t * Temp.t) option
val is_label : assem -> Label.t option
val rename : (Temp.t -> Temp.t) -> assem -> assem

val to_string : assem -> string
