open Core.Std

open Label
open Tree

let dump_relop = function
  | Eq -> "=="
  | Ne -> "!="
  | Lt -> "<"
  | Gt -> ">"
  | Le -> "<="
  | Ge -> ">="

let dump_binop = function
  | Plus -> "+"
  | Minus -> "-"
  | Times -> "*"
  | Div -> "/"
  | And -> "&"
  | Or -> "|"
  | Xor -> "^"

let rec dump_exp = function
  | Const i -> string_of_int i
  | Name l -> Label.to_string l
  | Temp t -> Temp.to_string t
  | Binop (a, o, b) ->
    "((" ^ dump_exp a ^ ")" ^ dump_binop o ^ "(" ^ dump_exp b ^ "))"
  | Mem e -> "*" ^ dump_exp e
  | Call (e, a) ->
    dump_exp e ^ "(" ^ String.concat ~sep:"," (List.map a dump_exp) ^ ")"
  | Eseq _ -> "ICE: Cgen.dump_exp used on uncanonicalized exp"

let dump_stm = function
  | Move (a, b) -> dump_exp a ^ " = " ^ dump_exp b ^ ";\n"
  | Exp e -> "(" ^ dump_exp e ^ ");\n"
  | Jump l -> "goto " ^ Label.to_string l ^ ";\n"
  | Cjump (a, o, b, t, e) ->
    "if (" ^ dump_exp a ^ dump_relop o ^ dump_exp b ^ ") {" ^
      "goto " ^ Label.to_string t ^ ";} else {" ^
      "goto " ^ Label.to_string e ^ ";}\n"
  | Nop -> ";"
  | Label l -> Label.to_string l ^ ":\n"
  | Seq _ -> "ICE: Cgen.dump_exp used on uncanonicalized stm"

let dump prg = print_endline (String.concat (List.map prg dump_stm))
