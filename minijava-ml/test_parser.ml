open Core.Std

open Typechecker

let parse (file_name: string) =
  In_channel.with_file file_name ~f:(fun ic ->
  let lexbuf = Lexing.from_channel ic in
    try
      Parser.main_stm Lexer.main lexbuf;
    with
    | Parser.Error ->
      let start_pos = lexbuf.Lexing.lex_start_p in
      let msg = Printf.sprintf
          "Parse error at line %i, character %i."
          (start_pos.Lexing.pos_lnum)
          (start_pos.Lexing.pos_cnum - start_pos.Lexing.pos_bol + 1) in
      failwith msg)

let usage_msg = "Usage: test_parser.byte file1.java [file2.java ...]\n"

let main =
  let no_argument = ref true in
  let print_ast file_name =
    let s = parse file_name in
    no_argument := false;
    print_endline "parse tree:";
    print_endline (Syntax.prg_to_string s);
    print_endline "class table:";
    (match compute_classtable s with
     | Ok tbl -> print_endline (classtable_to_string tbl)
     | Error msg -> print_endline ("Error: " ^ msg));
    print_endline "semantic checker:";
    match check_prg s with
    | Ok _ -> print_endline "OK"
    | Error msg -> print_endline ("Error: " ^ msg); exit 1
  in
  Arg.parse [] print_ast "test_parser file1.java [file2.java ...]";
  if !no_argument then print_string usage_msg
