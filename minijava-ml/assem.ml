open Core.Std

open Label

let word_size = 4

type mem = { base: Temp.t option;
             scale: int option;
             index: Temp.t option;
             displacement: int; } with sexp

let mem_make base =
  { base = Some base; scale = None; index = None; displacement = 0 }
    
let mem_make_indirect base scale displacement =
  { base = Some base; scale = Some scale; index = None; displacement }

type operand =
  | Imm of int
  | Reg of Temp.t
  | Mem of mem
with sexp

let string_from_op = function
  | Imm i -> string_of_int i
  | Reg t -> Temp.to_string t
  | Mem {base; scale; index; displacement} ->
    "DWORD PTR [" ^ (
      match base, scale, index, displacement with
      | Some b, _, None, 0 -> Temp.to_string b
      | Some b, Some 1, None, d -> Temp.to_string b ^ "+" ^ string_of_int d
      | Some b, Some -1, None, d -> Temp.to_string b ^ "-" ^ string_of_int d
      | _, _, _, _ -> failwith "ICE: invalid memory reference to be outputted")
    ^ "]"

let rename_op sigma = function
  | Imm i -> Imm i
  | Reg r -> Reg (sigma r)
  | Mem m ->
    Mem { m with
      base = Option.map m.base sigma;
      index = Option.map m.index sigma; }

let use_op = function
  | Imm _ -> []
  | Reg r -> [r]
  | Mem {base; index} ->
    List.append
      (match base with | Some b -> [b] | None -> []) 
      (match index with | Some i -> [i] | None -> [])      

type assem =  
  | Assem_jump of jump_kind * cond option * operand option * Label.t option
  | Assem_unary_op of unary_kind * operand
  | Assem_label of Label.t
  | Assem_binary_op of binary_kind * operand * operand
  | Assem_instr of instr_kind
and jump_kind = JMP | J | CALL
and unary_kind = PUSH | POP | NEG | NOT | INC | DEC | IDIV | ENTER
and binary_kind = MOV | ADD | SUB | IMUL | SHL | SHR | SAL | SAR | AND | OR | XOR | TEST | CMP | LEA
and instr_kind = RET | LEAVE | NOP | CDQ
and cond = E | NE | L | LE | G | GE | Z
with sexp

let eax = Temp.named "%eax"
let ebx = Temp.named "%ebx"
let ecx = Temp.named "%ecx"
let edx = Temp.named "%edx"
let esi = Temp.named "%esi"
let edi = Temp.named "%edi"
let esp = Temp.named "%esp"
let ebp = Temp.named "%ebp"
let free_reg = [eax; ebx; ecx; edx; esi; edi]
let all_reg = esp :: ebp :: free_reg
let unneeded_reg = Temp.named "%unneeded"

let use = function
  | Assem_jump (_, _, Some op, _) -> use_op op
  | Assem_jump _ -> []
  | Assem_unary_op (_, op) -> use_op op
  | Assem_label _ -> []
  | Assem_binary_op (MOV, Reg _, op2) -> use_op op2
  | Assem_binary_op (MOV, op1, op2) -> List.append (use_op op1) (use_op op2)
  | Assem_binary_op (_, op1, op2) -> List.append (use_op op1) (use_op op2)
  | Assem_instr _ -> []

let def = function
  | Assem_jump (CALL, _, _, _) -> [eax; ecx; edx]
  | Assem_jump _ -> []
  | Assem_unary_op (POP, op) -> use_op op
  | Assem_unary_op (NEG, op) -> use_op op
  | Assem_unary_op (NOT, op) -> use_op op
  | Assem_unary_op (INC, op) -> use_op op
  | Assem_unary_op (DEC, op) -> use_op op
  | Assem_unary_op (IDIV, op) -> eax :: edx :: use_op op
  | Assem_unary_op _ -> []
  | Assem_label _ -> []
  | Assem_binary_op (MOV, op1, (Mem _ as op2)) -> List.append (use_op op1) (use_op op2)
  | Assem_binary_op (MOV, op1, _) -> use_op op1
  | Assem_binary_op (ADD, op1, _) -> use_op op1
  | Assem_binary_op (SUB, op1, _) -> use_op op1
  | Assem_binary_op (SHL, op1, _) -> use_op op1
  | Assem_binary_op (SHR, op1, _) -> use_op op1
  | Assem_binary_op (SAL, op1, _) -> use_op op1
  | Assem_binary_op (SAR, op1, _) -> use_op op1
  | Assem_binary_op (AND, op1, _) -> use_op op1
  | Assem_binary_op (OR, op1, _) -> use_op op1
  | Assem_binary_op (XOR, op1, _) -> use_op op1
  | Assem_binary_op (LEA, op1, _) -> use_op op1
  | Assem_binary_op _ -> []
  | Assem_instr CDQ -> [edx]
  | Assem_instr _ -> []

let jumps = function
  | Assem_jump (JMP, _, _, Some l) -> [l]
  | Assem_jump (J,   _, _, Some l) -> [l]
  | Assem_jump _ -> []
  | _ -> []

let is_fall_through = function
  | Assem_jump (JMP, _, _, _)
  | Assem_instr RET
  | Assem_instr LEAVE -> false
  | _ -> true

let is_move_between_temps = function
  | Assem_binary_op (MOV, Reg dst, Reg src) -> Some (dst, src)
  | _ -> None

let is_label = function
  | Assem_label l -> Some l
  | _ -> None

let rename sigma it = match it with
  | Assem_jump (k, c, Some op, l) ->
    Assem_jump (k, c, Some (rename_op sigma op), l)
  | Assem_jump _ -> it
  | Assem_unary_op (k, op) ->
    Assem_unary_op (k, rename_op sigma op)
  | Assem_label _ -> it
  | Assem_binary_op (k, op1, op2) ->
    Assem_binary_op (k, rename_op sigma op1, rename_op sigma op2)
  | Assem_instr _ -> it


let to_string = function
  | Assem_jump (JMP, None, None, Some l) -> "\tJMP " ^ Label.to_string l
  | Assem_jump (J, Some c, None, Some l) -> "\tJ" ^ Sexp.to_string (sexp_of_cond c) ^ " " ^ Label.to_string l
  | Assem_jump (CALL, None, None, Some l) -> "\tCALL " ^ Label.to_string l
  | Assem_jump _ -> failwith "ICE: invalid jump generated"
  | Assem_unary_op (k, op) -> "\t" ^ Sexp.to_string (sexp_of_unary_kind k) ^ " " ^ string_from_op op
  | Assem_label l -> Label.to_string l ^ ":"
  | Assem_binary_op (IMUL, op1, Imm i) -> "\tIMUL " ^ string_from_op op1 ^ ", " ^ string_from_op op1 ^ ", " ^ string_of_int i
  | Assem_binary_op (k, op1, op2) -> "\t" ^ Sexp.to_string (sexp_of_binary_kind k) ^ " " ^ string_from_op op1 ^ ", " ^ string_from_op op2
  | Assem_instr k -> "\t" ^ Sexp.to_string (sexp_of_instr_kind k)
