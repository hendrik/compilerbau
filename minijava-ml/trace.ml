open Core.Std
module Map = Core.Core_map

open Tree
open Label

module BlockTable = Map.Make (String)

let (++) = List.append

let rec trace' acc fini = function
  | Jump l1 :: Label l2 :: ss ->
    (acc ++ [Jump l1]) :: trace' [Label l2] fini ss

  | Cjump (a, o, b, l1, l2) :: Label l3 :: ss ->
    (acc ++ [Cjump (a, o, b, l1, l2)]) :: trace' [Label l3] fini ss

  | s :: Label l :: ss ->
    trace' (acc ++ [s]) fini (Jump l :: Label l :: ss)

  | Jump l1 :: _ :: ss ->
    (* Eat dead code.  *)
    trace' acc fini (Jump l1 :: ss)

  | s :: ss ->
    trace' (acc ++ [s]) fini ss

  | [] -> [acc ++ [Jump fini]]

let trace =
  let fini = Label.named "fini" in
  trace' [Label (Label.named "init")] fini

let rec peep_jumps = function
  | Jump l1 :: Label l2 :: ss when l1 = l2 -> Label l2 :: peep_jumps ss
  | s :: ss -> s :: peep_jumps ss
  | [] -> []

let reorder blocks =
  let blocktbl =
    List.fold blocks ~init:BlockTable.empty ~f:fun tbl e ->
      match e with
      | Label l :: _ -> BlockTable.add tbl ~key:(Label.to_string l) ~data:e
      | _ -> failwith "ICE: unlabeled block"
  in
  let rec sort tbl here =
    match BlockTable.find tbl here with
    | None -> (tbl, [])   (* already emitted *)
    | Some blk ->
      let tbl' = (BlockTable.remove tbl here) in
      match List.last_exn blk with
      | Jump l ->
        let (tbl'', res) = sort tbl' (Label.to_string l) in
        (tbl'', blk ++ res)
      | Cjump (_, _, _, l_then, l_else) ->
        let (tbl'', res1) = sort tbl' (Label.to_string l_else) in
        let (tbl''', res2) = sort tbl'' (Label.to_string l_then) in
        (tbl''', blk ++ res1 ++ res2)
      | _ -> failwith "ICE: block ends without jump"
  in
  peep_jumps (snd (sort blocktbl "init"))

let trace_and_reorder x =
  let fini = Label.make () in
  List.tl_exn (reorder (trace' [Label (Label.named "init")] fini x)) ++
    [Label fini]
