open Core.Std

open Assem

let rec final_peep = function
  | Assem_jump (JMP, _, _, Some l1) :: Assem_label l2 :: xs when l1 = l2 ->
    Assem_label l2 :: final_peep xs
  | Assem_instr NOP :: xs ->
    final_peep xs
  | Assem_binary_op (MOV, a, b) :: xs when a = b ->
    final_peep xs
  | Assem_binary_op (MOV, Reg a, _) :: xs when a = unneeded_reg ->
    final_peep xs
  | Assem_binary_op (MOV, a, _) :: Assem_binary_op (MOV, b, c) :: xs
      when a = b && a <> c ->
    Assem_binary_op (MOV, b, c) :: final_peep xs
  | Assem_binary_op (MOV, Reg a, Imm 0) :: xs ->
    Assem_binary_op (XOR, Reg a, Reg a) :: final_peep xs
  | x :: xs ->
    x :: final_peep xs
  | [] ->
    []
