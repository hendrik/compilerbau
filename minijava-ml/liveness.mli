open Frame
open Assem

val allocate_registers : frame -> assem list -> frame * assem list
