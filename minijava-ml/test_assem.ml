open Core.Std

open Assem
open Label

let _ =
  let op = Assem_unary_op (IMUL, Reg (Temp.named "%t")) in
  print_endline (Sexp.to_string_hum (sexp_of_assem op))
