open Core.Std
open Tree
open Assem

type frame

val mk_frame : string -> int -> frame
val param : frame -> int -> tree_exp

val alloc_local : frame -> frame * tree_exp
val alloc_stack : frame -> frame * operand

val frame : frame * assem list -> assem list
