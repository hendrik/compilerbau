#!/bin/sh
set -e

check() {
  msg=$1; shift
  if eval "$@" >/dev/null; then
    echo ok - $msg
  else
    echo fail - $msg
  fi
}

for f in ../MiniJava-Beispiele/ShouldFail/TypeErrors/*.java; do
  check "detect type error in $f" "! ./test_parser.byte $f 2>/dev/null"
done
