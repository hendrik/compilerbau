open Core.Std

type ty =
  | Ty_arr of ty
  | Ty_bool
  | Ty_class of string
  | Ty_int

and exp =
  | Exp_array_get of exp * exp
  | Exp_array_length of exp
  | Exp_bin_op of exp * op * exp
  | Exp_false
  | Exp_id of string
  | Exp_int_const of int
  | Exp_invoke of exp * ty option ref * string * exp list
  | Exp_neg of exp
  | Exp_new of string
  | Exp_new_int_array of exp
  | Exp_this
  | Exp_true
and op = Plus | Minus | Times | Div | And | Lt

and stm =
  | Stm_array_assign of string * exp * exp
  | Stm_assign of string * exp
  | Stm_if of exp * stm * stm
  | Stm_list of stm list
  | Stm_print_char of exp
  | Stm_println_int of exp
  | Stm_while of exp * stm

with sexp

type declclass = { class_name : string;
                   super_name : string option;
                   fields : declvar list;
                   methods : declmeth list; }

and declmain = { main_class_name : string;
                 main_arg : string;
                 main_arg_type : string;
                 main_body : stm; }

and declmeth = { return_ty : ty;
                 method_name : string;
                 parameters : parameter list;
                 local_vars : declvar list;
                 body : stm;
                 return_exp : exp; }

and declvar = { var_ty : ty;
                name : string; }

and parameter = { id : string;
                  par_ty : ty }
with sexp

type prg = { main_class : declmain;
             classes : declclass list; }
with sexp

let prg_to_string p = Sexp.to_string_hum (sexp_of_prg p)

let ty_to_string t = Sexp.to_string_hum (sexp_of_ty t)
