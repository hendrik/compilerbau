#include "classes.hpp"
#include <sstream>

class exp_printer : public exp_visitor
{
public:
  VISITOR_CLASS(EXP_CLASSES)

  std::string print(const visitable_type&);
  std::string print(const visitable_type* e)
    { return print(*e); }
  template <class... Args>
  std::string print(const std::unique_ptr<Args...>& p)
    { return print(*p); }

private:
  std::ostringstream output;
};

class stm_printer : public stm_visitor
{
public:
  VISITOR_CLASS(STM_CLASSES)

  std::string print(const visitable_type&);
  std::string print(const visitable_type* s)
    { return print(*s); }
  template <class... Args>
  std::string print(const std::unique_ptr<Args...>& p)
    { return print(*p); }

private:
  std::ostringstream output;
};

