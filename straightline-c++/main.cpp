#include "interpreter.hpp"
#include "printer.hpp"
#include <iostream>

int main()
{
  assign_stm s{"a", op_exp{num_exp{5}, binop::plus, num_exp{3}}};
  std::vector<exp_ptr> l1;
  l1.emplace_back(exp_ptr{new id_exp{"a"}});
  l1.emplace_back(exp_ptr{new op_exp{id_exp{"a"}, binop::minus, num_exp{1}}});
  compound_stm s2{assign_stm{"a", op_exp{num_exp{5}, binop::plus, num_exp{3}}},
                 assign_stm{"b", eseq_exp{print_stm{move(l1)},
                                          op_exp{num_exp{10}, binop::mul, id_exp{"a"}}}}};

  stm_printer p;
  std::cout << "Program 1:\n" << p.print(s) << std::endl;

  state_type state;
  stm_interpreter stmi{state};
  std::cout << "Output:\n";
  stmi.run(s);
  std::cout << "\nResult:\n";
  for (const auto& st : state)
    std::cout << st.first << " = " << st.second << std::endl;

  std::cout << std::endl << "Program 2:\n" << p.print(s2) << std::endl;

  state.clear();
  std::cout << "Output:\n";
  stmi.run(s2);
  std::cout << "\nResult:\n";
  for (const auto& st : state)
    std::cout << st.first << " = " << st.second << std::endl;
};
