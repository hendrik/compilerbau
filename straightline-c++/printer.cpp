#include "printer.hpp"

using namespace std;

void
exp_printer::visit(const eseq_exp& e)
{
  stm_printer pstm;
  exp_printer exp;
  output << "(" << pstm.print(e.stm.get()) << ", " << exp.print(e.exp) << ")";
}

void
exp_printer::visit(const id_exp& v)
{
  output << v.id;
}

void
exp_printer::visit(const num_exp& v)
{
  output << v.num;
}

void
exp_printer::visit(const op_exp& v)
{
  exp_printer exp;
  output << exp.print(v.left) << " " << v.op << " " << exp.print(v.right);
}

string
exp_printer::print(const visitable_type& exp)
{
  output.str("");
  exp.accept(this);
  return output.str();
}

void
stm_printer::visit(const assign_stm& s)
{
  exp_printer pexp;
  output << s.id << " := " << pexp.print(s.exp);
}

void
stm_printer::visit(const compound_stm& s)
{
  stm_printer pstm;
  output << pstm.print(s.stm1) << "; " << pstm.print(s.stm2);
}

void
stm_printer::visit(const print_stm& s)
{
  output << "print (";
  exp_printer pexp;
  auto it = begin(s.exps);
  output << pexp.print(*it);
  for (++it; it != end(s.exps); ++it)
    output << ", " << pexp.print(*it);
  output << ")";
}

string
stm_printer::print(const visitable_type& s)
{
  output.str("");
  s.accept(this);
  return output.str();
}
