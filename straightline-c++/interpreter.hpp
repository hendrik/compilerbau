#include "classes.hpp"
#include <map>

using state_type = std::map<std::string, int>;

class exp_interpreter : public exp_visitor
{
public:
  VISITOR_CLASS(EXP_CLASSES)

  exp_interpreter(state_type* s)
    : state{s} {}
  exp_interpreter(state_type& s)
    : state{&s} {}

  int run(const exp& e)
    { val = 0; e.accept(this); return val; }
  int run(const exp& e, state_type& s)
    { state = &s; return run(e); }
  template <class... Args>
  int run(const std::unique_ptr<Args...>& p)
    { return run(*p); }

private:
  state_type* state;
  int val;
};

class stm_interpreter : public stm_visitor
{
public:
  VISITOR_CLASS(STM_CLASSES)

  stm_interpreter(state_type* s)
    : state{s} {}
  stm_interpreter(state_type& s)
    : state{&s} {}

  state_type& run(const stm& e)
    { e.accept(this); return *state; }
  state_type& run(const stm& e, state_type& s)
    { state = &s; return run(e); }
  template <class... Args>
  state_type& run(const std::unique_ptr<Args...>& p)
    { return run(*p); }

private:
  state_type* state;
};

