#include "interpreter.hpp"
#include <functional>
#include <iostream>

using namespace std;

void
exp_interpreter::visit(const eseq_exp& e)
{
  stm_interpreter stmi{state};
  exp_interpreter expi{state};
  stmi.run(e.stm);
  val = expi.run(e.exp);
}

void
exp_interpreter::visit(const op_exp& e)
{
  const static map<binop, function<int (int, int)>> ops{
    { binop::plus, plus<int>() },
    { binop::minus, minus<int>() },
    { binop::mul, multiplies<int>() },
    { binop::div, divides<int>() }
  };
  exp_interpreter expi{state};

  val = ops.find(e.op)->second(expi.run(e.left), expi.run(e.right));
}

void
exp_interpreter::visit(const num_exp& e)
{
  val = e.num;
}

void
exp_interpreter::visit(const id_exp& e)
{
  val = (*state)[e.id];
}

void
stm_interpreter::visit(const assign_stm& e)
{
  exp_interpreter expi{state};
  (*state)[e.id] = expi.run(e.exp);
}

void
stm_interpreter::visit(const compound_stm& e)
{
  stm_interpreter stmi{state};
  stmi.run(e.stm1);
  stmi.run(e.stm2);
}

void
stm_interpreter::visit(const print_stm& e)
{
  exp_interpreter expi{state};
  for (const auto& exp : e.exps)
    cout << expi.run(exp) << " ";
  cout.flush();
}
