#ifndef CLASSES_HPP
#define CLASSES_HPP

#include "visitor.hpp"

#include <memory>
#include <string>
#include <type_traits>
#include <vector>

struct eseq_exp;
struct id_exp;
struct num_exp;
struct op_exp;
struct assign_stm;
struct compound_stm;
struct print_stm;

#define EXP_CLASSES eseq_exp, id_exp, num_exp, op_exp
#define STM_CLASSES assign_stm, compound_stm, print_stm

using exp = visitable<EXP_CLASSES>;
using stm = visitable<STM_CLASSES>;

template <class T>
using exp_visitable = visitable_impl<T, EXP_CLASSES>;
template <class T>
using stm_visitable = visitable_impl<T, STM_CLASSES>;

using exp_visitor = visitor<EXP_CLASSES>;
using stm_visitor = visitor<STM_CLASSES>;

using exp_ptr = std::unique_ptr<const exp>;
using stm_ptr = std::unique_ptr<const stm>;

struct eseq_exp : public exp_visitable<eseq_exp>
{
  template <class T, class U>
  eseq_exp(T&& t, U&& u)
    : stm{new T(std::forward<T>(t))},
      exp{new U(std::forward<U>(u))}
  {}

  stm_ptr stm;
  exp_ptr exp;
};

struct id_exp : public exp_visitable<id_exp>
{
  template <class... Args>
  id_exp(Args&&... args)
    : id{std::forward<Args>(args)...} {}

  const std::string id;
};

struct num_exp : public exp_visitable<num_exp>
{
  num_exp(int n) : num{n} {}
  const int num;
};

enum class binop
{
  plus,
  minus,
  div,
  mul
};

template <class T, class Trait>
std::basic_ostream<T, Trait>&
operator<<(std::basic_ostream<T, Trait>& s, binop op)
{
  switch (op)
    {
    case binop::plus:
      s << "+";
      break;
    case binop::minus:
      s << "-";
      break;
    case binop::mul:
      s << "*";
      break;
    case binop::div:
      s << "/";
      break;
    }

  return s;
}

struct op_exp : public exp_visitable<op_exp>
{
  template <class T, class U>
  op_exp(T&& l, binop o, U&& r)
    : left{new T(std::forward<T>(l))}, op{o},
      right{new U(std::forward<U>(r))} {}
  exp_ptr left;
  const binop op;
  exp_ptr right;
};

struct assign_stm : public stm_visitable<assign_stm>
{
  template <class T, class U>
  assign_stm(T&& t, U&& u)
    : id{std::forward<T>(t)}, exp{new U(std::forward<U>(u))} {}

  const std::string id;
  exp_ptr exp;
};

struct compound_stm : public stm_visitable<compound_stm>
{
  template <class T, class U>
  compound_stm(T&& t, U&& u)
    : stm1{new T(std::forward<T>(t))},
      stm2{new U(std::forward<U>(u))} {}

  stm_ptr stm1;
  stm_ptr stm2;
};

struct print_stm : public stm_visitable<print_stm>
{
public:
  template <class... Args>
  print_stm(Args&&... args)
    : exps{std::forward<Args>(args)...} {}

  std::vector<exp_ptr> exps;
};

#endif /* CLASSES_HPP */
