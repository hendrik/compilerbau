#ifndef MINIJAVA_PRINTER_HPP
#define MINIJAVA_PRINTER_HPP

#include "minijava.hpp"
#include <iostream>

class prettyprinter : public minijava::stm_visitor,
                      public minijava::exp_visitor,
                      public minijava::type_visitor
{
public:
  void print(minijava::goal& e)
    {
      print(*e.main);
      for (auto& f : e.classes)
        {
          indent_level = 0;
          newline();
          print(*f);
        }
      newline();
    }
  void print(minijava::main_class& e)
    {
      s << "class ";
      print(*e.name);
      s << " {";
      ++indent_level; newline();
      s << "public static void main (String[] ";
      print(*e.argv_name);
      s << ") {";
      ++indent_level; newline();
      print(*e.stm);
      --indent_level; newline();
      s << "}";
      --indent_level; newline();
    }

  void print(minijava::class_declaration& e)
    {
      s << "class ";
      print(*e.name);
      if (! e.parent->empty())
        {
          s << " extends ";
          print(*e.parent);
        }
      s << " {";
      ++indent_level; newline();
      print_array(e.vars);
      newline();
      print_array(e.methods);
      --indent_level; newline();
      s << "}";
    }
  void print(minijava::var_declaration& e)
    {
      print(*e.type);
      s << " ";
      print(*e.name);
      s << ";";
    }

  void print(minijava::method_declaration& e)
    {
      s << "public ";
      print(*e.return_type);
      s << " ";
      print(*e.name);
      s << "(";
      for (auto& e : e.args)
        {
          print(*e.first);
          s << " ";
          print(*e.second);
          s << ", ";
        }
      s << ") {";
      ++indent_level; newline();
      print_array(e.vars);
      newline();
      print_array(e.stms);
      newline();
      s << "return ";
      print(*e.return_exp);
      s << ";";
      --indent_level; newline();
      s << "}";
    }

  void print(minijava::identifier& e)
    { s << e; }
  void print(const minijava::stm& e)
    { e.accept(*this); }
  void print(const minijava::type& e)
    { e.accept(*this); }
  void print(const minijava::exp& e)
    { e.accept(*this); }

  void visit(const minijava::int_array_type&) override
    { s << "int []"; }
  void visit(const minijava::bool_type&) override
    { s << "boolean"; }
  void visit(const minijava::int_type&) override
    { s << "int"; }
  void visit(const minijava::class_type& e) override
    { print(*e.name); }

  void visit(const minijava::scope_stm& e) override
    {
      s << "{";
      ++indent_level; newline();
      print_array(e.stms);
      --indent_level; newline();
      s << "}";
    }
  void visit(const minijava::conditional_stm& e) override
    {
      s << "if (";
      print(*e.cond);
      s << ")";
      ++indent_level; newline();
      print(*e.if_stm);
      --indent_level; newline();
      s << "else";
      ++indent_level; newline();
      print(*e.else_stm);
      --indent_level; newline();
    }
  void visit(const minijava::loop_stm& e) override
    {
      s << "while (";
      print(*e.cond);
      s << ")";
      ++indent_level; newline();
      print(*e.body);
      --indent_level; newline();
    }
  void visit(const minijava::println_stm& e) override
    {
      s << "System.out.println(";
      print(*e.exp);
      s << ");";
    }
  void visit(const minijava::print_stm& e) override
    {
      s << "System.out.print((char)";
      print(*e.exp);
      s << ");";
    }
  void visit(const minijava::assign_stm& e) override
    {
      print(*e.name);
      s << " = ";
      print(*e.value);
      s << ";";
    }
  void visit(const minijava::array_assign_stm& e) override
    {
      print(*e.name);
      s << "[";
      print(*e.num);
      s << "] = ";
      print(*e.value);
      s << ";";
    }

  void visit(const minijava::logical_and_exp& e) override
    {
      print(*e.left);
      s << " && ";
      print(*e.right);
    }
  void visit(const minijava::smaller_than_exp& e) override
    {
      print(*e.left);
      s << " < ";
      print(*e.right);
    }
  void visit(const minijava::plus_exp& e) override
    {
      print(*e.left);
      s << " + ";
      print(*e.right);
    }
  void visit(const minijava::minus_exp& e) override
    {
      print(*e.left);
      s << " - ";
      print(*e.right);
    }
  void visit(const minijava::times_exp& e) override
    {
      print(*e.left);
      s << " * ";
      print(*e.right);
    }
  void visit(const minijava::div_exp& e) override
    {
      print(*e.left);
      s << " / ";
      print(*e.right);
    }
  void visit(const minijava::array_access_exp& e) override
    {
      print(*e.id);
      s << "[";
      print(*e.pos);
      s << "]";
    }
  void visit(const minijava::length_exp& e) override
    {
      print(*e.id);
      s << ".length";
    }
  void visit(const minijava::function_exp& e) override
    {
      print(*e.id);
      s << ".";
      print(*e.func);
      s << "(";
      for (const auto& f : e.args)
        { if (f == nullptr) continue; print(*f); s << ", "; }
      s << ")";
    }
  void visit(const minijava::num_exp& e) override
    { s << *e.num; }
  void visit(const minijava::true_exp&) override
    { s << "true"; }
  void visit(const minijava::false_exp&) override
    { s << "false"; }
  void visit(const minijava::variable_exp& e) override
    { print(*e.id); }
  void visit(const minijava::this_exp&) override
    { s << "this"; }
  void visit(const minijava::array_new_exp& e) override
    { s << "new int ["; print(*e.size); s << "]"; }
  void visit(const minijava::new_exp& e) override
    { s << "new "; print(*e.type); s << "()"; }
  void visit(const minijava::not_exp& e) override
    { s << "!"; print(*e.exp); }
  void visit(const minijava::braces_exp& e) override
    { s << "("; print(*e.exp); s << ")"; }

  std::ostream& s = std::cout;

private:
  int indent_level = 0;
  const char *const indent = "\t";

  void newline()
    {
      s << std::endl;
      for (int i = 0; i < indent_level; ++i)
        s << indent;
    }

  template <class T>
  void print_array(const minijava::array_type<T>& arr)
    {
      for (const auto& e : arr)
        {
          print(*e);
          newline();
        }
    }
};

#endif /* MINIJAVA_PRINTER_HPP */
