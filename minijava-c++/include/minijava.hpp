#ifndef MINIJAVA_MINIJAVA_HPP
#define MINIJAVA_MINIJAVA_HPP

#include "visitor.hpp"

#include <memory>
#include <string>
#include <vector>

namespace minijava
{
  template <class T>
  using ptr_type = std::unique_ptr<T>;
  template <class T>
  using base_array_type = std::vector<T>;
  template <class T>
  using array_type = base_array_type<ptr_type<T>>;
  using integer_type = long;

  struct goal;
  struct main_class;
  struct class_declaration;
  struct var_declaration;
  struct method_declaration;
  using identifier = std::string;

  using goal_ptr = ptr_type<goal>;
  using class_decl_ptr = ptr_type<class_declaration>;

  struct int_array_type;
  struct bool_type;
  struct int_type;
  struct class_type;
#define TYPE_CLASSES int_array_type, bool_type, int_type, class_type
  using type = visitable<TYPE_CLASSES>;
  using type_visitor = visitor<TYPE_CLASSES>;
  using type_ptr = ptr_type<type>;

  struct scope_stm;
  struct conditional_stm;
  struct loop_stm;
  struct println_stm;
  struct print_stm;
  struct assign_stm;
  struct array_assign_stm;
#define STATEMENT_CLASSES scope_stm, conditional_stm, loop_stm, println_stm, print_stm, assign_stm, array_assign_stm
  using stm = visitable<STATEMENT_CLASSES>;
  using stm_visitor = visitor<STATEMENT_CLASSES>;
  using stm_ptr = ptr_type<stm>;

  struct logical_and_exp;
  struct smaller_than_exp;
  struct plus_exp;
  struct minus_exp;
  struct times_exp;
  struct div_exp;
  struct array_access_exp;
  struct length_exp;
  struct function_exp;
  struct num_exp;
  struct true_exp;
  struct false_exp;
  struct variable_exp;
  struct this_exp;
  struct array_new_exp;
  struct new_exp;
  struct not_exp;
  struct braces_exp;
#define EXPRESSION_CLASSES logical_and_exp, smaller_than_exp, plus_exp, minus_exp, times_exp, div_exp, array_access_exp, length_exp, function_exp, num_exp, true_exp, false_exp, variable_exp, this_exp, array_new_exp, new_exp, not_exp, braces_exp
  using exp = visitable<EXPRESSION_CLASSES>;
  using exp_visitor = visitor<EXPRESSION_CLASSES>;
  using exp_ptr = ptr_type<exp>;

};

#include "minijava/expression.hpp"
#include "minijava/statement.hpp"
#include "minijava/type.hpp"
#include "minijava/general.hpp"

#endif /* MINIJAVA_MINIJAVA_HPP */
