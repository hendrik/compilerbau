#ifndef MINIJAVA_OUTPUT_HPP
#define MINIJAVA_OUTPUT_HPP

#include <string>

std::string token_to_string(int);

#endif /* MINIJAVA_OUTPUT_HPP */
