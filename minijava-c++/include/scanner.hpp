#ifndef MINIJAVA_SCANNER_HPP
#define MINIJAVA_SCANNER_HPP

#ifndef yyFlexLexerOnce
#include <FlexLexer.h>
#endif

#undef YY_DECL
#define YY_DECL int minijava::scanner::yylex()

#include "parser.tab.hpp"

namespace minijava
{
  class scanner : public yyFlexLexer
  {
  public:
    scanner(std::istream* in = nullptr,
            std::ostream* out = nullptr)
      : yyFlexLexer{in, out} {}

    int yylex();
    int yylex(parser::semantic_type* lval, parser::location_type* lloc)
    {
      yylval = lval;
      yylloc = lloc;
      return yylex();
    }

    minijava::goal* root;

  private:
    parser::semantic_type* yylval;
    parser::location_type* yylloc;
  };
};

#endif /* MINIJAVA_SCANNER_HPP */
