#ifndef MINIJAVA_MINIJAVA_GENERAL_HPP
#define MINIJAVA_MINIJAVA_GENERAL_HPP

namespace minijava
{

  struct goal
  {
    template <class T, class... Args>
    goal(T&& m, Args&&... cs)
      : main{ptr_type<main_class>{std::forward<T>(m)}},
        classes{std::forward<Args>(cs)...} {}
    ptr_type<main_class> main;
    array_type<class_declaration> classes;
  };

  struct main_class
  {
    ptr_type<identifier> name;
    ptr_type<identifier> argv_name;
    ptr_type<stm> stm;
  };

  struct class_declaration
  {
    ptr_type<identifier> name;
    ptr_type<identifier> parent;
    array_type<var_declaration> vars;
    array_type<method_declaration> methods;
  };

  struct var_declaration
  {
    ptr_type<type> type;
    ptr_type<identifier> name;
  };

  struct method_declaration
  {
    ptr_type<type> return_type;
    ptr_type<identifier> name;
    base_array_type<std::pair<ptr_type<type>, ptr_type<identifier>>> args;
    array_type<var_declaration> vars;
    array_type<stm> stms;
    ptr_type<exp> return_exp;
  };

};

#endif /* MINIJAVA_MINIJAVA_GENERAL_HPP */

