#ifndef MINIJAVA_STATEMENT_HPP
#define MINIJAVA_STATEMENT_hPP

#include "minijava.hpp"

namespace minijava
{

  template <class T>
  using stm_visitable = visitable_impl<T, STATEMENT_CLASSES>;

  struct scope_stm : public stm_visitable<scope_stm>
  {
    template <class... Args>
    scope_stm(Args&&... args)
      : stms{std::forward<Args>(args)...} {}

    array_type<stm> stms;
  };

  struct conditional_stm : public stm_visitable<conditional_stm>
  {
    template <class U, class V, class W>
    conditional_stm(U&& u, V&& v, W&& w)
      : cond{std::forward<U>(u)}, if_stm{std::forward<V>(v)},
        else_stm{std::forward<W>(w)} {}

    ptr_type<exp> cond;
    ptr_type<stm> if_stm;
    ptr_type<stm> else_stm;
  };

  struct loop_stm : public stm_visitable<loop_stm>
  {
    template <class U, class V>
    loop_stm(U&& u, V&& v)
      : cond{std::forward<U>(u)}, body{std::forward<V>(v)}
    {}

    ptr_type<exp> cond;
    ptr_type<stm> body;
  };

  struct println_stm : public stm_visitable<println_stm>
  {
    template <class... Args>
    println_stm(Args&&... args)
      : exp{std::forward<Args>(args)...} {}

    ptr_type<exp> exp;
  };

  struct print_stm : public stm_visitable<print_stm>
  {
    template <class... Args>
    print_stm(Args&&... args)
      : exp{std::forward<Args>(args)...} {}

    ptr_type<exp> exp;
  };

  struct assign_stm : public stm_visitable<assign_stm>
  {
    template <class U, class V>
    assign_stm(U&& u, V&& v)
      : name{std::forward<U>(u)}, value{std::forward<V>(v)}
    {}

    ptr_type<identifier> name;
    ptr_type<exp> value;
  };

  struct array_assign_stm : public stm_visitable<array_assign_stm>
  {
    template <class U, class V, class W>
    array_assign_stm(U&& u, V&& v, W&& w)
      : name{std::forward<U>(u)}, num{std::forward<V>(v)},
        value{std::forward<W>(w)} {}

    ptr_type<identifier> name;
    ptr_type<exp> num;
    ptr_type<exp> value;
  };

};

#endif /* MINIJAVA_STATEMENT_HPP */
