#ifndef MINIJAVA_TYPE_HPP
#define MINIJAVA_TYPE_HPP

namespace minijava
{

  template <class T>
  using type_visitable = visitable_impl<T, TYPE_CLASSES>;

  struct int_array_type : public type_visitable<int_array_type> {};
  struct bool_type : public type_visitable<bool_type> {};
  struct int_type : public type_visitable<int_type> {};
  struct class_type : public type_visitable<class_type>
  {
    template <class... Args>
    class_type(Args&&... args)
      : name{std::forward<Args>(args)...} {}

    ptr_type<identifier> name;
  };

};

#endif /* MINIJAVA_TYPE_HPP */
