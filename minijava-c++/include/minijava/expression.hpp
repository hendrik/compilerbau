#ifndef MINIJAVA_MINIJAVA_EXPRESSION_HPP
#define MINIJAVA_MINIJAVA_EXPRESSION_HPP

namespace minijava
{
  template <class T>
  using exp_visitable = visitable_impl<T, EXPRESSION_CLASSES>;

  struct logical_and_exp : public exp_visitable<logical_and_exp>
  {
    template <class U, class V>
    logical_and_exp(U&& u, V&& v)
      : left{std::forward<U>(u)}, right{std::forward<V>(v)} {}

    ptr_type<exp> left;
    ptr_type<exp> right;
  };

  struct smaller_than_exp : public exp_visitable<smaller_than_exp>
  {
    template <class U, class V>
    smaller_than_exp(U&& u, V&& v)
      : left{std::forward<U>(u)}, right{std::forward<V>(v)} {}

    ptr_type<exp> left;
    ptr_type<exp> right;
  };

  struct plus_exp : public exp_visitable<plus_exp>
  {
    template <class U, class V>
    plus_exp(U&& u, V&& v)
      : left{std::forward<U>(u)}, right{std::forward<V>(v)} {}

    ptr_type<exp> left;
    ptr_type<exp> right;
  };

  struct minus_exp : public exp_visitable<minus_exp>
  {
    template <class U, class V>
    minus_exp(U&& u, V&& v)
      : left{std::forward<U>(u)}, right{std::forward<V>(v)} {}

    ptr_type<exp> left;
    ptr_type<exp> right;
  };

  struct times_exp : public exp_visitable<times_exp>
  {
    template <class U, class V>
    times_exp(U&& u, V&& v)
      : left{std::forward<U>(u)}, right{std::forward<V>(v)} {}

    ptr_type<exp> left;
    ptr_type<exp> right;
  };

  struct div_exp : public exp_visitable<div_exp>
  {
    template <class U, class V>
    div_exp(U&& u, V&& v)
      : left{std::forward<U>(u)}, right{std::forward<V>(v)} {}

    ptr_type<exp> left;
    ptr_type<exp> right;
  };

  struct array_access_exp : public exp_visitable<array_access_exp>
  {
    template <class U, class V>
    array_access_exp(U&& u, V&& v)
      : id{std::forward<U>(u)}, pos{std::forward<V>(v)} {}

    ptr_type<exp> id;
    ptr_type<exp> pos;
  };

  struct length_exp : public exp_visitable<length_exp>
  {
    template <class... Args>
    length_exp(Args&&... args)
      : id{std::forward<Args>(args)...} {}

    ptr_type<exp> id;
  };

  struct function_exp : public exp_visitable<function_exp>
  {
    template <class U, class V, class W>
    function_exp(U&& u, V&& v, W&& w)
      : id{std::forward<U>(u)}, func{std::forward<V>(v)},
        args{std::forward<W>(w)} {}

    ptr_type<exp> id;
    ptr_type<identifier> func;
    array_type<exp> args;
  };

  struct num_exp : public exp_visitable<num_exp>
  {
    template <class... Args>
    num_exp(Args&&... args)
      : num{std::forward<Args>(args)...} {}

    ptr_type<integer_type> num;
  };

  struct true_exp : public exp_visitable<true_exp> {};
  struct false_exp : public exp_visitable<false_exp> {};

  struct variable_exp : public exp_visitable<variable_exp>
  {
    template <class... Args>
    variable_exp(Args&&... args)
      : id{std::forward<Args>(args)...} {}

    ptr_type<identifier> id;
  };

  struct this_exp : public exp_visitable<this_exp> {};

  struct array_new_exp : public exp_visitable<array_new_exp>
  {
    template <class... Args>
    array_new_exp(Args&&... args)
      : size{std::forward<Args>(args)...} {}

    ptr_type<exp> size;
  };

  struct new_exp : public exp_visitable<new_exp>
  {
    template <class... Args>
    new_exp(Args&&... args)
      : type{std::forward<Args>(args)...} {}

    ptr_type<identifier> type;
  };

  struct not_exp : public exp_visitable<not_exp>
  {
    template <class... Args>
    not_exp(Args&&... args)
      : exp{std::forward<Args>(args)...} {}

    ptr_type<exp> exp;
  };

  struct braces_exp : public exp_visitable<braces_exp>
  {
    template <class... Args>
    braces_exp(Args&&... args)
      : exp{std::forward<Args>(args)...} {}

    ptr_type<exp> exp;
  };

};

#endif /* MINIJAVA_MINIJAVA_EXPRESSION_HPP */
