#include "minijava.hpp"
#include "output.hpp"
#include "printer.hpp"
#include "scanner.hpp"

#include <fstream>
#include <iostream>

int
main(int argc, char** argv)
{
  if (argc < 2)
    return 1;
  std::ifstream in{argv[1]};
  minijava::scanner scanner{&in};
  minijava::parser parser{scanner};

  /*
  parser.set_debug_stream(std::cout);
  parser.set_debug_level(1);
  */

  auto p = parser.parse();

  std::unique_ptr<minijava::goal> root{scanner.root};

  prettyprinter printer;
  printer.print(*root);
  return p;
}
