#include "output.hpp"

std::string
token_to_string(int c)
{
  switch (c)
    {
    default: return std::to_string(c);
    case 258: return "Asterisk";
    case 259: return "Boolean";
    case 260: return "Char";
    case 261: return "Class";
    case 262: return "Comma";
    case 263: return "Dot";
    case 264: return "Else";
    case 265: return "Eof";
    case 266: return "Equals";
    case 267: return "Extends";
    case 268: return "False";
    case 269: return "Identifier";
    case 270: return "If";
    case 271: return "Int";
    case 272: return "Integer";
    case 273: return "Left_braces";
    case 274: return "Left_brackets";
    case 275: return "Left_parenthesis";
    case 276: return "Length";
    case 277: return "Logical_and";
    case 278: return "Lower_than";
    case 279: return "Main";
    case 280: return "Minus";
    case 281: return "New";
    case 282: return "Plus";
    case 283: return "Public";
    case 284: return "Return";
    case 285: return "Right_braces";
    case 286: return "Right_brackets";
    case 287: return "Right_parenthesis";
    case 288: return "Semicolon";
    case 289: return "Slash";
    case 290: return "Static";
    case 291: return "System_out_print";
    case 292: return "System_out_println";
    case 293: return "This";
    case 294: return "True";
    case 295: return "Void";
    case 296: return "While";
    }
}

