%{
#include "scanner.hpp"
#include "parser.tab.hpp"
%}

%option yyclass="scanner" noyywrap c++ nounput debug

INTEGRAL                [a-zA-Z][-_a-zA-Z0-9]*
INTEGER                 [0-9]+

%{
#define YY_USER_ACTION yylloc->columns(yyleng);
%}

%%
%{
  yylloc->step();
%}

"!"                     { return minijava::parser::token::Bang; }
"&&"                    { return minijava::parser::token::And_and; }
"("                     { return minijava::parser::token::Left_parenthesis; }
")"                     { return minijava::parser::token::Right_parenthesis; }
"*"                     { return minijava::parser::token::Asterisk; }
"+"                     { return minijava::parser::token::Plus; }
","                     { return minijava::parser::token::Comma; }
"-"                     { return minijava::parser::token::Minus; }
"."                     { return minijava::parser::token::Dot; }
"/"                     { return minijava::parser::token::Slash; }
";"                     { return minijava::parser::token::Semicolon; }
"<"                     { return minijava::parser::token::Lower_than; }
"="                     { return minijava::parser::token::Equals; }
"["                     { return minijava::parser::token::Left_brackets; }
"]"                     { return minijava::parser::token::Right_brackets; }
"{"                     { return minijava::parser::token::Left_braces; }
"}"                     { return minijava::parser::token::Right_braces; }

"System.out.print"      { return minijava::parser::token::System_out_print; }
"System.out.println"    { return minijava::parser::token::System_out_println; }
"boolean"               { return minijava::parser::token::Boolean; }
"char"                  { return minijava::parser::token::Char; }
"class"                 { return minijava::parser::token::Class; }
"else"                  { return minijava::parser::token::Else; }
"extends"               { return minijava::parser::token::Extends; }
"false"                 { return minijava::parser::token::False; }
"if"                    { return minijava::parser::token::If; }
"int"                   { return minijava::parser::token::Int; }
"length"                { return minijava::parser::token::Length; }
"main"                  { return minijava::parser::token::Main; }
"new"                   { return minijava::parser::token::New; }
"public"                { return minijava::parser::token::Public; }
"return"                { return minijava::parser::token::Return; }
"static"                { return minijava::parser::token::Static; }
"this"                  { return minijava::parser::token::This; }
"true"                  { return minijava::parser::token::True; }
"void"                  { return minijava::parser::token::Void; }
"while"                 { return minijava::parser::token::While; }

"/*"                    {
                          for (;;)
                            {
                              int c = yyinput();
                              if (c == '\n')
				{
				  yylloc->lines(yyleng);
				  yylloc->step();
				}
                              else if (c == EOF)
                                throw std::runtime_error("Found EOF during comment.");
                              else if (c == '*')
                                {
                                  while ((c = yyinput()) == '*');
                                  if (c == '/')
                                    break;
                                }
                            }
                        }
"//"                    {
                           for (int c = yyinput(); c != '\n' && c != EOF;
                                c = yyinput());
                           yylloc->lines(yyleng); yylloc->step();
                        }

"\n"                    { yylloc->lines(yyleng); yylloc->step(); }
[ \t]+                  { yylloc->step(); }
{INTEGRAL}              {
                          yylval->sval = new std::string{yytext};
                          return minijava::parser::token::Identifier;
                        }
{INTEGER}               {
                          yylval->ival = atol(yytext);
			  return minijava::parser::token::Integer;
			}
<<EOF>>                 { return minijava::parser::token::Eof; }
